<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKtnVideoTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ktn_video_type', function(Blueprint $table)
		{
            $table->increments('id');
			$table->string('name', 135)->nullable();
			$table->integer('updatedby')->nullable();
			$table->dateTime('updateddate')->nullable();
			$table->string('source', 450)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ktn_video_type');
	}

}
