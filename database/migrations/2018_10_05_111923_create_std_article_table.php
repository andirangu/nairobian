<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStdArticleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('std_article', function(Blueprint $table)
		{
            $table->increments('id');
			$table->string('dcx_id', 165);
			$table->char('source', 39)->nullable();
            $table->char('storyType', 255)->nullable();
			$table->integer('categoryid');
			$table->integer('categoryid_1');
			$table->integer('homepagelistorder')->nullable();
			$table->integer('parentcategorylistorder')->nullable();
			$table->integer('listorder')->nullable();
			$table->boolean('articlestatusid');
			$table->string('author', 150)->nullable();
			$table->dateTime('posteddate')->nullable();
			$table->dateTime('publishdate')->nullable();
			$table->date('publishday')->nullable();
			$table->string('title', 450)->nullable();
			$table->boolean('inactive', 1)->nullable();
			$table->string('summary', 3000)->nullable();
			$table->string('thumbURL', 600)->nullable();
			$table->string('thumbcaption', 150)->nullable();
			$table->string('keywords', 600)->nullable();
			$table->string('glancefacts', 3000)->nullable();
			$table->text('story', 65535)->nullable();
			$table->integer('noofhits')->nullable();
			$table->integer('noofshares')->nullable();
			$table->integer('noofcomments')->nullable();
			$table->integer('createdby')->nullable();
			$table->dateTime('updateddate')->nullable();
			$table->integer('updatedby')->nullable();
			$table->integer('oldid')->nullable();
			$table->integer('oldcatid')->nullable();
			$table->integer('published')->nullable();
			$table->string('long_title', 1200)->nullable();
			$table->string('linked_category', 1500)->nullable();
			$table->string('breaking', 15)->nullable();
			$table->integer('related_video')->nullable();
			$table->integer('author_id')->nullable();
			$table->integer('slideshowid')->nullable();
			$table->boolean('mainlatestlistorder')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('std_article');
	}

}
