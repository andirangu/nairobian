<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKtnVideoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ktn_video', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('categoryid');
			$table->dateTime('publishdate')->nullable();
			$table->integer('listorder')->nullable();
			$table->dateTime('posteddate')->nullable();
			$table->string('title', 300)->nullable();
			$table->string('longtitle', 1350)->nullable();
			$table->string('videoURL', 300)->nullable();
			$table->string('keywords', 600)->nullable();
			$table->integer('reporter')->nullable();
			$table->boolean('inactive', 1)->nullable();
			$table->integer('noofhits')->nullable();
			$table->integer('noofmoderatedcomments')->nullable();
			$table->integer('noofunmoderatedcomments')->nullable();
			$table->text('description', 65535)->nullable();
			$table->integer('createdby')->nullable();
			$table->integer('updatedby')->nullable();
			$table->dateTime('updateddate')->nullable();
			$table->integer('homepagelistorder')->nullable();
			$table->char('video_ready', 3)->nullable();
			$table->string('platform', 360)->nullable();
			$table->string('lemonwhaleURL', 1500)->nullable();
			$table->integer('producer')->nullable();
			$table->string('videothumb', 600)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ktn_video');
	}

}
