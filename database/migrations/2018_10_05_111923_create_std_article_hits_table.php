<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStdArticleHitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('std_article_hits', function(Blueprint $table)
		{
			$table->increments('std_article_hits_id');
			$table->integer('std_article_hits_article_id');
			$table->integer('std_article_hits')->nullable();
			$table->dateTime('std_article_hits_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('std_article_hits');
	}

}
