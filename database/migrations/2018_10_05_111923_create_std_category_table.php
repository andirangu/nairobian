<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStdCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('std_category', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('refer_id')->nullable();
			$table->char('site', 39)->nullable();
			$table->integer('parentid')->nullable();
			$table->string('name', 300)->nullable();
			$table->string('shortname', 90)->nullable();
			$table->string('cat_imagename', 300)->nullable();
			$table->boolean('inactive', 1)->nullable();
			$table->integer('layoutid')->nullable();
			$table->integer('colourthemeid')->nullable();
			$table->integer('listorder')->nullable();
			$table->integer('noofvideos')->nullable();
			$table->boolean('hideinparentcategory', 1)->nullable();
			$table->integer('updatedby')->nullable();
			$table->dateTime('updateddate')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('std_category');
	}

}
