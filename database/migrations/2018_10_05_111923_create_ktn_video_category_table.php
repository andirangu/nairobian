<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKtnVideoCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ktn_video_category', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('videotypeid')->nullable();
			$table->string('name', 135)->nullable();
			$table->boolean('inactive', 1)->nullable();
			$table->boolean('showintopnews', 1)->nullable();
			$table->integer('updatedby')->nullable();
			$table->dateTime('updateddate')->nullable();
			$table->integer('listorder')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ktn_video_category');
	}

}
