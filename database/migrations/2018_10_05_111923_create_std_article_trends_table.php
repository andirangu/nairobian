<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStdArticleTrendsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('std_article_trends', function(Blueprint $table)
		{
			$table->increments('std_trend_id');
			$table->integer('std_trend_article_id')->nullable();
			$table->integer('std_trend_hits')->nullable();
			$table->dateTime('std_trend_timestamp')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('std_article_trends');
	}

}
