<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Oct 2018 06:05:24 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KtnVideoType
 *
 * @property int $id
 * @property string|null $name
 * @property int $updatedby
 * @property \Carbon\Carbon|null $updateddate
 * @property string|null $source
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoType whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoType whereUpdatedby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoType whereUpdateddate($value)
 * @mixin \Eloquent
 */
class KtnVideoType extends Model
{
	protected $table = 'ktn_video_type';
	public $timestamps = false;

	protected $casts = [
		'updatedby' => 'int'
	];

	protected $dates = [
		'updateddate'
	];

	protected $fillable = [
		'name',
		'updatedby',
		'updateddate',
		'source'
	];
}
