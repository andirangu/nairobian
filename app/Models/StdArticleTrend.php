<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Oct 2018 06:05:24 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StdArticleTrend
 *
 * @property int $std_trend_id
 * @property int $std_trend_article_id
 * @property int $std_trend_hits
 * @property \Carbon\Carbon|null $std_trend_timestamp
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticleTrend whereStdTrendArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticleTrend whereStdTrendHits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticleTrend whereStdTrendId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticleTrend whereStdTrendTimestamp($value)
 * @mixin \Eloquent
 */
class StdArticleTrend extends Model
{
	protected $primaryKey = 'std_trend_id';
	public $timestamps = false;

	protected $casts = [
		'std_trend_article_id' => 'int',
		'std_trend_hits' => 'int'
	];

	protected $dates = [
		'std_trend_timestamp'
	];

	protected $fillable = [
		'std_trend_article_id',
		'std_trend_hits',
		'std_trend_timestamp'
	];
}
