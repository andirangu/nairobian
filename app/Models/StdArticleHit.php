<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Oct 2018 06:05:24 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StdArticleHit
 *
 * @property int $std_article_hits_id
 * @property int $std_article_hits_article_id
 * @property int $std_article_hits
 * @property \Carbon\Carbon|null $std_article_hits_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticleHit whereStdArticleHits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticleHit whereStdArticleHitsArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticleHit whereStdArticleHitsDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticleHit whereStdArticleHitsId($value)
 * @mixin \Eloquent
 */
class StdArticleHit extends Model
{
	protected $primaryKey = 'std_article_hits_id';
	public $timestamps = false;

	protected $casts = [
		'std_article_hits_article_id' => 'int',
		'std_article_hits' => 'int'
	];

	protected $dates = [
		'std_article_hits_date'
	];

	protected $fillable = [
		'std_article_hits_article_id',
		'std_article_hits',
		'std_article_hits_date'
	];

    public function popular()
    {
        return $this->hasMany(StdArticleHit::class, 'std_article_hits_article_id');
    }
}
