<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Oct 2018 06:05:24 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KtnVideo
 *
 * @property int $id
 * @property int $categoryid
 * @property \Carbon\Carbon|null $publishdate
 * @property int $listorder
 * @property \Carbon\Carbon|null $posteddate
 * @property string|null $title
 * @property string|null $longtitle
 * @property string|null $videoURL
 * @property string|null $keywords
 * @property int $reporter
 * @property bool $inactive
 * @property int $noofhits
 * @property int $noofmoderatedcomments
 * @property int $noofunmoderatedcomments
 * @property string|null $description
 * @property int $createdby
 * @property int $updatedby
 * @property \Carbon\Carbon|null $updateddate
 * @property int $homepagelistorder
 * @property string|null $video_ready
 * @property string|null $platform
 * @property string|null $lemonwhaleURL
 * @property int $producer
 * @property string|null $videothumb
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereCategoryid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereCreatedby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereHomepagelistorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereInactive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereLemonwhaleURL($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereListorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereLongtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereNoofhits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereNoofmoderatedcomments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereNoofunmoderatedcomments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo wherePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo wherePosteddate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereProducer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo wherePublishdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereReporter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereUpdatedby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereUpdateddate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereVideoReady($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereVideoURL($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideo whereVideothumb($value)
 * @mixin \Eloquent
 */
class KtnVideo extends Model
{
	protected $table = 'ktn_video';
	public $timestamps = false;

	protected $casts = [
		'categoryid' => 'int',
		'listorder' => 'int',
		'reporter' => 'int',
		'inactive' => 'bool',
		'noofhits' => 'int',
		'noofmoderatedcomments' => 'int',
		'noofunmoderatedcomments' => 'int',
		'createdby' => 'int',
		'updatedby' => 'int',
		'homepagelistorder' => 'int',
		'producer' => 'int'
	];

	protected $dates = [
		'publishdate',
		'posteddate',
		'updateddate'
	];

	protected $fillable = [
		'categoryid',
		'publishdate',
		'listorder',
		'posteddate',
		'title',
		'longtitle',
		'videoURL',
		'keywords',
		'reporter',
		'inactive',
		'noofhits',
		'noofmoderatedcomments',
		'noofunmoderatedcomments',
		'description',
		'createdby',
		'updatedby',
		'updateddate',
		'homepagelistorder',
		'video_ready',
		'platform',
		'lemonwhaleURL',
		'producer',
		'videothumb'
	];
}
