<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Oct 2018 06:05:24 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StdArticle
 *
 * @property int $id
 * @property string|null $dcx_id
 * @property string|null $source
 * @property int $categoryid
 * @property int $categoryid_1
 * @property int $homepagelistorder
 * @property int $parentcategorylistorder
 * @property int $listorder
 * @property bool $articlestatusid
 * @property string|null $author
 * @property \Carbon\Carbon|null $posteddate
 * @property \Carbon\Carbon|null $publishdate
 * @property \Carbon\Carbon|null $publishday
 * @property string|null $title
 * @property bool $inactive
 * @property string|null $summary
 * @property string|null $thumbURL
 * @property string|null $thumbcaption
 * @property string|null $keywords
 * @property string|null $glancefacts
 * @property string|null $story
 * @property int $noofhits
 * @property int $noofshares
 * @property int $noofcomments
 * @property int $createdby
 * @property \Carbon\Carbon|null $updateddate
 * @property int $updatedby
 * @property int $oldid
 * @property int $oldcatid
 * @property int $published
 * @property string|null $long_title
 * @property string|null $linked_category
 * @property string|null $breaking
 * @property int $related_video
 * @property int $author_id
 * @property int $slideshowid
 * @property bool $mainlatestlistorder
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereArticlestatusid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereBreaking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereCategoryid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereCategoryid1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereCreatedby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereDcxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereGlancefacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereHomepagelistorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereInactive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereLinkedCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereListorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereLongTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereMainlatestlistorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereNoofcomments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereNoofhits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereNoofshares($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereOldcatid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereOldid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereParentcategorylistorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle wherePosteddate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle wherePublishdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle wherePublishday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereRelatedVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereSlideshowid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereStory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereThumbURL($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereThumbcaption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereUpdatedby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdArticle whereUpdateddate($value)
 * @mixin \Eloquent
 */
class StdArticle extends Model
{
    protected $table = 'std_article';
    public $timestamps = false;

    protected $casts = [
        'categoryid' => 'int',
        'categoryid_1' => 'int',
        'homepagelistorder' => 'int',
        'parentcategorylistorder' => 'int',
        'listorder' => 'int',
        'articlestatusid' => 'bool',
        'inactive' => 'bool',
        'noofhits' => 'int',
        'noofshares' => 'int',
        'noofcomments' => 'int',
        'createdby' => 'int',
        'updatedby' => 'int',
        'oldid' => 'int',
        'oldcatid' => 'int',
        'published' => 'int',
        'related_video' => 'int',
        'author_id' => 'int',
        'slideshowid' => 'int',
        'mainlatestlistorder' => 'bool'
    ];

    protected $dates = [
        'posteddate',
        'publishdate',
        'publishday',
        'updateddate'
    ];

    protected $fillable = [
        'dcx_id',
        'source',
        'categoryid',
        'categoryid_1',
        'homepagelistorder',
        'parentcategorylistorder',
        'listorder',
        'articlestatusid',
        'author',
        'posteddate',
        'publishdate',
        'publishday',
        'title',
        'inactive',
        'summary',
        'thumbURL',
        'thumbcaption',
        'keywords',
        'glancefacts',
        'story',
        'noofhits',
        'noofshares',
        'noofcomments',
        'createdby',
        'updateddate',
        'updatedby',
        'oldid',
        'oldcatid',
        'published',
        'long_title',
        'linked_category',
        'breaking',
        'related_video',
        'author_id',
        'slideshowid',
        'mainlatestlistorder'
    ];

    public function category()
    {
        return $this->belongsTo(StdCategory::class, 'categoryid');
    }

    public function hits()
    {
        return $this->hasMany(StdArticleHit::class, 'std_article_hits_article_id');
    }
}
