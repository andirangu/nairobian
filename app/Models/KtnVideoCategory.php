<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Oct 2018 06:05:24 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KtnVideoCategory
 *
 * @property int $id
 * @property int $videotypeid
 * @property string|null $name
 * @property bool $inactive
 * @property bool $showintopnews
 * @property int $updatedby
 * @property \Carbon\Carbon|null $updateddate
 * @property int $listorder
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoCategory whereInactive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoCategory whereListorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoCategory whereShowintopnews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoCategory whereUpdatedby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoCategory whereUpdateddate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KtnVideoCategory whereVideotypeid($value)
 * @mixin \Eloquent
 */
class KtnVideoCategory extends Model
{
	protected $table = 'ktn_video_category';
	public $timestamps = false;

	protected $casts = [
		'videotypeid' => 'int',
		'inactive' => 'bool',
		'showintopnews' => 'bool',
		'updatedby' => 'int',
		'listorder' => 'int'
	];

	protected $dates = [
		'updateddate'
	];

	protected $fillable = [
		'videotypeid',
		'name',
		'inactive',
		'showintopnews',
		'updatedby',
		'updateddate',
		'listorder'
	];
}
