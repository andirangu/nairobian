<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Oct 2018 06:05:24 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StdCategory
 *
 * @property int $id
 * @property int $refer_id
 * @property string|null $site
 * @property int $parentid
 * @property string|null $name
 * @property string|null $shortname
 * @property string|null $cat_imagename
 * @property bool $inactive
 * @property int $layoutid
 * @property int $colourthemeid
 * @property int $listorder
 * @property int $noofvideos
 * @property bool $hideinparentcategory
 * @property int $updatedby
 * @property \Carbon\Carbon|null $updateddate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereCatImagename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereColourthemeid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereHideinparentcategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereInactive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereLayoutid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereListorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereNoofvideos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereParentid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereReferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereShortname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereUpdatedby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StdCategory whereUpdateddate($value)
 * @mixin \Eloquent
 */
class StdCategory extends Model
{
    protected $table = 'std_category';
    public $timestamps = false;

    protected $casts = [
        'refer_id' => 'int',
        'parentid' => 'int',
        'inactive' => 'bool',
        'layoutid' => 'int',
        'colourthemeid' => 'int',
        'listorder' => 'int',
        'noofvideos' => 'int',
        'hideinparentcategory' => 'bool',
        'updatedby' => 'int'
    ];

    protected $dates = [
        'updateddate'
    ];

    protected $fillable = [
        'refer_id',
        'site',
        'parentid',
        'name',
        'shortname',
        'cat_imagename',
        'inactive',
        'layoutid',
        'colourthemeid',
        'listorder',
        'noofvideos',
        'hideinparentcategory',
        'updatedby',
        'updateddate'
    ];

    public function subCategories()
    {
        return StdCategory::whereSite('nairobian')
            ->whereInactive('Null')
            ->whereParentid($this->id)
            ->get();
    }
}
