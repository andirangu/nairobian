<?php
/**
 * Created by PhpStorm.
 * User: genicho
 * Date: 10/18/18
 * Time: 8:46 AM
 */

namespace App\Composers;

use App\Models\StdCategory;
use Illuminate\View\View;

class ChildMenuComposer
{

    public function compose(View $view)
    {

        $childNavs = StdCategory::where('site', 'nairobian')
            ->where('inactive', 'Null')
            ->where('parentid', '!=', 0)
            ->get();

        $view->with('childNavs', $childNavs);
    }

}
