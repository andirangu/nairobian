<?php
/**
 * Created by PhpStorm.
 * User: genicho
 * Date: 10/18/18
 * Time: 8:44 AM
 */

namespace App\Composers;

use App\Models\StdCategory;
use Illuminate\View\View;

class MoreMenuComposer
{

    public function compose(View $view)
    {
        $limit = 20;

        $moreNavs = StdCategory::where('site', 'nairobian')
            ->where('inactive', 'Null')
            ->where("parentid", 0)
            ->where('id', '>=', 6)
            ->take($limit)
            ->get();

        $view->with('moreNavs', $moreNavs);
    }

}
