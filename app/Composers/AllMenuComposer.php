<?php

namespace App\Composers;


use App\Models\StdCategory;
use Illuminate\View\View;

class AllMenuComposer
{

    public function compose(View $view)
    {
        $limit = 50;

        $menus = StdCategory::where('site', 'nairobian')
            ->where('inactive', 'Null')
            ->where("parentid", 0)
            ->take($limit)
            ->get();
        $view->with('menus', $menus);
    }


}
