<?php


use App\Composers\ChildMenuComposer;
use App\Composers\MenuComposer;
use App\Composers\MoreMenuComposer;

view()->composer('partials.header', MenuComposer::class);
view()->composer('partials.header', MoreMenuComposer::class);
view()->composer('partials.header', ChildMenuComposer::class);
