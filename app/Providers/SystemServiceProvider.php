<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SystemServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->requireFiles();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     *
     */
    private function requireFiles()
    {
        $files = ['helpers', 'composers'];
        foreach ($files as $file) require_once app_path("Library/$file.php");
    }
}
