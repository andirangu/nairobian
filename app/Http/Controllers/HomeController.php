<?php

namespace App\Http\Controllers;

use App\Models\KtnVideo;
use App\Models\StdArticle;

use View;

class HomeController extends Controller
{

    private $data;

    public function __construct()
    {


        View::composers([
            'App\Composers\MenuComposer' => ['index'] //attaches HomeComposer to home.blade.php
        ]);


        View::composers([
            'App\Composers\AllMenuComposer' => ['index'] //attaches HomeComposer to home.blade.php
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = $this->getArticle(1);
        $gossip = $this->getArticle(3);
        $ted = $this->getArticle(4);
        $betting = $this->getArticle(5);
        $story = $this->getArticle(6);
        $city = $this->getArticle(8);
        $politics = $this->getArticle(9);
        $health = $this->getArticle(10);
        $tbt = $this->getArticle(11);
        $sport = $this->getArticle(12);
        $fashion = $this->getArticle(13);


        $cities = $this->getArticles(8, 3);
        $politicsCard = $this->getArticles(9, 4);
        $healthCard = $this->getArticles(10, 4);
        $myStoryCard = $this->getArticles(6, 4);
        $gossips = $this->getArticles(3, 5);

        //fashion to act as events
        $events = $this->getArticles(13, 16);
        $uncles = $this->getArticles(4, 6);

        $tbts = $this->getArticles(11, 2);
        $sports = $this->getArticles(12, 2);
        $fashions = $this->getArticles(13, 3);
        $foods = $this->getArticles(15, 2);


        //subcategories
        $breakingNews = $this->getArticles(38, 10);

        $videos = $this->getVideos(10);

        $moreTbts = $this->getMoreArticles(15, 7);


        $bettings = $this->getArticles(5, 3);
        $moneys = $this->getArticles(16, 3);
        $nairobiShops = $this->getArticles(17, 6);
        $defenders = $this->getArticles(18, 5);

        $this->data['news'] = $news;
        $this->data['gossip'] = $gossip;
        $this->data['ted'] = $ted;
        $this->data['betting'] = $betting;
        $this->data['story'] = $story;
        $this->data['city'] = $city;
        $this->data['politics'] = $politics;
        $this->data['health'] = $health;
        $this->data['tbt'] = $tbt;
        $this->data['sport'] = $sport;
        $this->data['fashion'] = $fashion;

        $this->data['cities'] = $cities;
        $this->data['politicsCard'] = $politicsCard;
        $this->data['healthCard'] = $healthCard;
        $this->data['myStoryCard'] = $myStoryCard;
        $this->data['gossips'] = $gossips;
        $this->data['events'] = $events;
        $this->data['uncles'] = $uncles;
        $this->data['tbts'] = $tbts;
        $this->data['sports'] = $sports;
        $this->data['fashions'] = $fashions;
        $this->data['foods'] = $foods;
        $this->data['moneys'] = $moneys;
        $this->data['bettings'] = $bettings;
        $this->data['nairobiShops'] = $nairobiShops;

        $this->data['defenders'] = $defenders;
        $this->data['moreTbts'] = $moreTbts;

        //subcat
        $this->data['breakingNews'] = $breakingNews;

        $this->data['videos'] = $videos;

        return view('index', ['data' => $this->data]);
    }


    public function getArticle($category)
    {
        $article = StdArticle::whereCategoryid($category)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->first();
        return $article;
    }

    public function getArticles($category, $limit)
    {
        $article = StdArticle::whereCategoryid($category)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->take($limit)
            ->get();
        return $article;
    }


    public function getMoreArticles($category, $limit)
    {
        $article = StdArticle::whereCategoryid($category)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->where('id', '>=', 3)
            ->take($limit)
            ->get();
        return $article;
    }


    public function getVideos()
    {
        $video = KtnVideo::orderby("publishdate", "DESC")
            ->take(5)
            ->get();

        return $video;
    }

}

