<?php

namespace App\Http\Controllers;

use App\Models\StdArticle;
use App\Models\StdCategory;
use Illuminate\Http\Request;
use View;

class CategoriesController extends Controller
{

    private $data;

    public function __construct()
    {

        View::composers([
            'App\Composers\MenuComposer' => ['index'] //attaches HomeComposer to home.blade.php
        ]);


        View::composers([
            'App\Composers\AllMenuComposer' => ['index'] //attaches HomeComposer to home.blade.php
        ]);

    }

    public function show($id)
    {
        $articles = StdArticle::whereCategoryid($id)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->skip(0)
            ->take(3)
            ->orderBy('publishdate', 'desc')
            ->get();


        $articles2 = StdArticle::whereCategoryid($id)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->skip(2)
            ->take(7)
            ->orderBy('publishdate', 'desc')
            ->get();

        $latestArticles = StdArticle::where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->orderBy('publishdate', 'desc')
            ->take(6)
            ->get();

        $events = StdArticle::whereCategoryid(13)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->orderBy('publishdate', 'desc')
            ->take(16)
            ->get();


        $defenderArticles = StdArticle::whereCategoryid(18)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->orderBy('publishdate', 'desc')
            ->take(8)
            ->get();


        $gossipArticles = StdArticle::whereCategoryid(3)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->orderBy('publishdate', 'desc')
            ->take(7)
            ->get();


        $loadmorearticles = StdArticle::whereCategoryid($id)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->skip(9)
            ->take(PHP_INT_MAX)
            ->orderBy('publishdate', 'desc')
            ->get();

        $populars = StdArticle::whereCategoryid($id)
            ->where('source', 'null')
            ->where('inactive', 'Null')
            ->orderBy('publishdate', 'desc')
            ->take(6)
            ->get();


        $nairobiShops = $this->getArticles(17, 6);

        $this->data['nairobiShops'] = $nairobiShops;

        $this->data['articles'] = collect($articles);
        $this->data['articles2'] = collect($articles2);

        return view('category', ['data' => $this->data])
            ->with('articles2', $articles2)
            ->with('loadmorearticles', $loadmorearticles)
            ->with('latestArticles', $latestArticles)
            ->with('defenderArticles', $defenderArticles)
            ->with('events', $events)
            ->with('gossipArticles', $gossipArticles)
            ->with('populars', $populars);


    }


    public function getArticles($category, $limit)
    {
        $article = StdArticle::whereCategoryid($category)
            ->where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->take($limit)
            ->get();
        return $article;
    }




    /*public function getTrendingArticles($limit)
    {
        $cut_of_date = date('Y-m-d H:i:s', mktime(date('H'), date('i') - 5, 0, date('m'), date('d'), date('Y')));
        $populars = "SELECT title, id, publishdate, summary,categoryid, thumbURL, SUM(std_trend_hits) AS std_trend_hits1,source,author 
FROM std_article, std_article_trends WHERE std_trend_timestamp>='$cut_of_date' AND std_article.id = std_article_trends.std_trend_article_id AND inactive IS NULL and 
(std_article.source='nairobian' or std_article.source='entertainment' or std_article.source='main') AND 
(std_article.title NOT LIKE '%intercourse%' AND std_article.title NOT LIKE '%between the sheets%') 
AND (std_article.keywords NOT LIKE '%intercourse%' AND std_article.keywords NOT LIKE '%between the sheets%')
 and std_article.categoryid NOT LIKE '289' GROUP BY std_article.id ORDER BY std_trend_hits1 ORDER BY std_trend_hits1";
        return $populars;
    }*/
}
