<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNavigation()
    {
        $dbh =
            $this->db1->where("site", "main")
                ->where("parentid", 0)
                ->where("inactive is NULL")
                ->get("std_category");
        if ($dbh) {
            return $dbh->result();
        } else {
            return json_encode($this->db1->error());
        }
    }

    public function getArticle($id)
    {
        $id = (int)$id;
        $this->article_hits($id);
        $dbh =
            $this->db1->where("id", $id)
                ->where("inactive is NULL")
                ->get("std_article");
        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->row();
            } else {
                $dbh = NULL;
                $dbh =
                    $this->db2->where("id", $id)
                        ->where("inactive is NULL")
                        ->get("std_article");
                if ($dbh) {
                    if ($dbh->num_rows() > 0) {
                        return $dbh->row();
                    } else {
                        $dbh =
                            $this->db3->where("id", $id)
                                ->where("inactive is NULL")
                                ->get("std_article");
                        if ($dbh) {
                            if ($dbh->num_rows() > 0) {
                                return $dbh->row();
                            } else {
                                return FALSE;
                            }
                        } else {
                            echo "db3";
                            return json_encode($this->db3->error());
                        }
                    }
                } else {
                    echo "db2";
                    return json_encode($this->db2->error());
                }
            }

        } else {
            return json_encode($this->db1->error());
        }
    }

    public function article_hits($articleID)
    {
        date_default_timezone_set('Africa/Nairobi');
        $cut_of_date = date('Y-m-d');
        $current_date_time = date('Y-m-d H:i:s');

        // if (rand(0, 200) % 100 == 0)
        // 	{

        $max_std_trend_time = date('Y-m-d H:i:s', mktime(date('H'), date('i') - 5, 0, date('m'), date('d'), date('Y')));
        $id = (int)$articleID;
        $id = $this->db1->escape_str($id);
        $query = "select * from std_article_hits  where std_article_hits_article_id ='$id' 
and std_article_hits_date >= '$cut_of_date 00:00:00'";
        $results = $this->db1->query($query);

        if (strpos($_SERVER['SERVER_NAME'], "standardmedia") > 0) {
            $query = "delete  from std_article_hits  where std_article_hits_date < '$cut_of_date 00:00:00'";
            $this->db1->query($query);
        }
        $total_stories = $results->num_rows();

        if ($total_stories > 0) {
            $query = "update std_article_hits  set std_article_hits = std_article_hits + 1 where std_article_hits_article_id = '$id' and std_article_hits_date >= '$cut_of_date 00:00:00'";
        } else {
            $query = "insert into  std_article_hits  (std_article_hits_article_id, std_article_hits, std_article_hits_date ) values ( '$id', 1, '$current_date_time') ";
        }

        $results = $this->db1->query($query);
        $query = "insert into  std_article_trends  (std_trend_article_id, std_trend_hits ,std_trend_timestamp   ) values ( '$id', 1, '$current_date_time' ) ";
        $results = $this->db1->query($query);
        //       	}

        // if (rand(0, 5000) % 4500 == 5)
        // 	{

        $max_std_trend_time = date('Y-m-d H:i:s', mktime(date('H'), date('i') - 5, 0, date('m'), date('d'), date('Y')));
        $query = "delete  from std_article_trends  where std_trend_timestamp <= '$max_std_trend_time'";
        $this->db1->query($query);
        // }
    }

    public function getCatName($catid)
    {
        return
            $this->db1->where("id", $catid)
                ->where("inactive is NULL")
                ->get("std_category")
                ->row()
                ->name;
    }

    public function getOthersite($sitename, $limit, $start = 0)
    {
        $dbh =
            $this->db1->where("source", $sitename)
                ->where("inactive is null")
                ->order_by("publishdate", "DESC")
                ->order_by("homepagelistorder", "ASC")
                ->order_by("listorder", "ASC")
                ->limit($limit, $start)
                ->get("std_article");
        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            }
        } else {
            print_r($this->db1->error());
            return (object)array("error" => $this->db1->error());
        }
    }

    public function getLatest($limit, $start, $id = array(1))
    {
        $dbh =
            $this->db1->where("inactive is null")
                ->where("source", 'main')
                ->where_not_in("id", $id)
                ->limit($limit, $start)
                ->get("std_article");
        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            }
        } else {
            return $this->db1->error();
        }
    }

    public function getHometop($limit, $start = 0)
    {
        $dbh =
            $this->db1->where("inactive is null")
                ->where("source", "main")
                ->where("publishdate <= now()")
                ->where("homepagelistorder is not null")
                ->order_by("publishday", "DESC")
                ->order_by("homepagelistorder", "asc")
                ->order_by("listorder", "asc")
                ->limit($limit, $start)
                ->get("std_article");
        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            }
        } else {
            return $this->db1->error();
        }
    }

    public function getCategoryTop($catid, $limit, $start = 0)
    {
        $dbh =
            $this->db1->select("std_article.*")
                ->join("std_category", "std_category.id=std_article.categoryid")
                ->where("std_article.inactive is null")
                ->where("source", "main")
                ->where("(std_category.id=" . $catid . " or std_category.parentid=" . $catid . " )")
                ->order_by("std_article.publishday", "DESC")
                ->order_by("std_article.parentcategorylistorder", "ASC")
                ->order_by("std_article.publishdate", "DESC")
                ->limit($limit, $start)
                ->get("std_article");
        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            } else {
                $dbh =
                    $this->db2->select("std_article.*")
                        ->join("std_category", "std_category.id=std_article.categoryid")
                        ->where("std_article.inactive is null")
                        ->where("source", "main")
                        ->where("(std_category.id=" . $catid . " or std_category.parentid=" . $catid . " )")
                        ->order_by("std_article.publishday", "DESC")
                        ->order_by("std_article.parentcategorylistorder", "ASC")
                        ->limit($limit, $start)
                        ->get("std_article");
                if ($dbh) {
                    if ($dbh->num_rows() > 0) {
                        return $dbh->result();
                    } else {
                        $dbh =
                            $this->db3->select("std_article.*")
                                ->join("std_category", "std_category.id=std_article.categoryid")
                                ->where("std_article.inactive is null")
                                ->where("source", "main")
                                ->where("(std_category.id=" . $catid . " or std_category.parentid=" . $catid . " )")
                                ->order_by("std_article.publishday", "DESC")
                                ->order_by("std_article.parentcategorylistorder", "ASC")
                                ->limit($limit, $start)
                                ->get("std_article");
                        if ($dbh->num_rows() > 0) {
                            return $dbh->result();
                        }
                    }
                }
            }
        } else {
            print_r($this->db1->error());
        }

    }

    public function getCategoryLatest($id, $notin, $limit, $start = 0)
    {
        $dbh =
            $this->db1->where("inactive is not null")
                ->where_not_in("id", $notin)
                ->where("source", "main")
                ->where("categoryid", $id)
                ->order_by("std_article.publishdate", "DESC")
                ->limit($limit, $start)
                ->get("std_article");
        if ($dbh->num_rows() > 0) {
            return $dbh->result();
        } else {
            $dbh =
                $this->db2->where("inactive is not null")
                    ->where_not_in("id", $notin)
                    ->where("source", "main")
                    ->where("categoryid", $id)
                    ->order_by("std_article.publishdate", "DESC")
                    ->limit($limit, $start)
                    ->get("std_article");
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            } else {
                $dbh =
                    $this->db3->where("inactive is not null")
                        ->where_not_in("id", $notin)
                        ->where("source", "main")
                        ->where("categoryid", $id)
                        ->order_by("std_article.publishdate", "DESC")
                        ->limit($limit, $start)
                        ->get("std_article");
                if ($dbh->num_rows() > 0) {
                    return $dbh->result();
                } else {

                }
            }
        }

    }

    public function getTrendingArticles($limit)
    {
        $cut_of_date = date('Y-m-d H:i:s', mktime(date('H'), date('i') - 5, 0, date('m'), date('d'), date('Y')));
        $sql = "
SELECT title, id, publishdate, summary,categoryid, thumbURL, SUM(std_trend_hits) AS std_trend_hits1,source,author 
FROM std_article, std_article_trends  WHERE std_trend_timestamp>='$cut_of_date' 
and std_article.id = std_article_trends.std_trend_article_id AND inactive 
IS NULL  and (std_article.source='main' or std_article.source='entertainment'
 or std_article.source='evewoman')  AND (std_article.title NOT LIKE '%intercourse%'
  AND std_article.title NOT LIKE '%between the sheets%') AND 
  (std_article.keywords NOT LIKE '%intercourse%' AND std_article.keywords NOT LIKE '%between the sheets%')  
  and std_article.categoryid NOT LIKE '289'  GROUP BY std_article.id ORDER BY std_trend_hits1 DESC LIMIT 0, $limit ";
        $result = $this->db1->query($sql);
        return $result->result();
    }

    public function checkForChild($catid)
    {
        $dbh =
            $this->db1->where("parentid", $catid)
                ->where("inactive is null")
                ->where("site", "main")
                ->get("std_category");
        if ($dbh->num_rows() > 0) {
            return (object)array("status" => TRUE, "data" => $dbh->result());
        } else {
            return (object)array("status" => FALSE);
        }
    }

    public function getTopVideos($source, $limit, $start = 0)
    {
        $dbh =
            $this->db1->select("ktn_video.*")
                ->join("ktn_video_category", "ktn_video_category.id=ktn_video.categoryid")
                ->join("ktn_video_type", "ktn_video_type.id=ktn_video_category.videotypeid")
                ->where("source", $source)
                ->where("ktn_video.inactive is null")
                ->order_by("ktn_video.publishdate", "DESC")
                ->limit($limit, $start)
                ->get("ktn_video");

        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            }
        } else {
            var_dump($this->db1->error());
        }
    }

    public function getLatestVideos($limit, $start = 0)
    {
        $dbh =
            $this->db1->select("ktn_video.*,source")
                ->join("ktn_video_category", "ktn_video_category.id=ktn_video.categoryid")
                ->join("ktn_video_type", "ktn_video_type.id=ktn_video_category.videotypeid")
                ->where("ktn_video.inactive is null")
                ->order_by("ktn_video.publishdate", "DESC")
                ->limit($limit, $start)
                ->get("ktn_video");

        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            }
        } else {
            var_dump($this->db1->error());
        }
    }

    public function relatedArticles($keywords, $id, $limit = 5)
    {
        $array_like = explode(';', $keywords);
        $like_statements = array();
        foreach ($array_like as $value) {
            $like_statements[] = "keywords LIKE '%" . addslashes($value) . "%'";
        }

        $like_string = "(" . implode(' OR ', $like_statements) . ")";
        $this->db1->where($like_string)
            ->where("inactive is null")
            ->where("source", "main")
            ->where_not_in('id', $id)
            ->limit($limit);
        $dbh = $this->db1->get("std_article");
        if ($dbh) {

            return $dbh->result();
        } else {
            print_r($this->db1_ > error());
        }
    }

    public function getProgrammeSchedule()
    {

    }

    public function getTopic($topic, $limit, $start = 0)
    {
        $topic = str_replace("-", " ", $topic);
        $dbh =
            $this->db1->where("inactive is null")
                ->like("keywords", $topic, "both")
                ->where("source", "main")
                ->limit($limit, $start)
                ->get("std_article");
        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            }
        } else {
            var_dump($this->db1->error());
        }
    }

    public function getAuthorContent($topic, $limit, $start = 0)
    {
        $topic = str_replace("-", " ", $topic);
        $dbh =
            $this->db1->where("inactive is null")
                ->like("author", $topic, "both")
                ->where("source", "main")
                ->limit($limit, $start)
                ->get("std_article");
        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->result();
            }
        } else {
            var_dump($this->db1->error());
        }
    }

    public function getCatId($id)
    {
        $dbh =
            $this->db1->select("id,parentid")
                ->where("id", $id)
                ->get("std_category");
        if ($dbh) {
            return $dbh->result();
        }
    }
}