<?php

namespace App\Http\Controllers;

use App\Models\StdArticle;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Input;
use Redirect;
use Request;
use Validator;
use View;

class LoginController extends Controller
{

    public function showLogin()
    {
        // show the form
        return View::make('login');
    }

    public function doLogin()
    {

        $rules = array(
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );

            if (Auth::attempt($userdata)) {

                return Redirect::to('/');

            } else {

                return back()->with('message', 'Wrong email and Password Combination');

            }

        }
    }


    public function doLogout()
    {
        Auth::logout();
        return Redirect::to('login');

    }

}

