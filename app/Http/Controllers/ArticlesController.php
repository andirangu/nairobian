<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StdArticle;
use View;

class ArticlesController extends Controller
{
    private $data;

    public function __construct()
    {


        View::composers([
            'App\Composers\MenuComposer' => ['index'] //attaches HomeComposer to home.blade.php
        ]);

        View::composers([
            'App\Composers\AllMenuComposer' => ['index'] //attaches HomeComposer to home.blade.php
        ]);

    }


    public function show($id)
    {
        $news = $this->getArticle(1);
        $schadals = $this->getArticle(2);
        $gossip = $this->getArticle(3);
        $ted = $this->getArticle(4);
        $betting = $this->getArticle(5);
        $story = $this->getArticle(6);
        /*$nairobian = $this->getArticle(7);*/
        $city = $this->getArticle(8);
        $politics = $this->getArticle(9);
        $health = $this->getArticle(10);
        $tbt = $this->getArticle(11);
        $sport = $this->getArticle(12);
        $fashion = $this->getArticle(13);

        $latests = $this->getLatest(5);
        $trendings = $this->getTrendingNews(5);


        $this->data['news'] = $news;
        $this->data['schadals'] = $schadals;
        $this->data['gossip'] = $gossip;
        $this->data['ted'] = $ted;
        $this->data['betting'] = $betting;
        $this->data['story'] = $story;
        $this->data['city'] = $city;
        $this->data['politics'] = $politics;
        $this->data['health'] = $health;
        $this->data['tbt'] = $tbt;
        $this->data['sport'] = $sport;
        $this->data['fashion'] = $fashion;

        $this->data['latests'] = $latests;
        $this->data['trendings'] = $trendings;

        $article = StdArticle::find($id);

        return view('article', ['data' => $this->data])->with('article', $article);
    }


    public function getArticle($category)
    {
        $article = StdArticle::whereCategoryid($category)->where('source', 'nairobian')
            ->orderBy('publishdate', 'desc')
            ->first();
        return $article;
    }

    public function getLatest($limit)
    {
        $article = StdArticle::where('source', 'nairobian')
            ->where('inactive', 'Null')
            ->orderBy('publishdate', 'desc')
            ->take($limit)
            ->get();
        return $article;
    }

    protected function getTrendingNews($limit)
    {
        $cut_of_date = date('Y-m-d H:i:s', mktime(date('H'), date('i') - 5, 0, date('m'), date('d'), date('Y')));
        $article = "SELECT title, id, publishdate, summary,categoryid, thumbURL, SUM(std_trend_hits) AS std_trend_hits1,source,author 
                    FROM std_article, std_article_trends  WHERE std_trend_timestamp>='$cut_of_date' 
                    and std_article.id = std_article_trends.std_trend_article_id AND inactive 
                    IS NULL  and std_article.source='nairobian' AND (std_article.title NOT LIKE '%intercourse%'
                    AND std_article.title NOT LIKE '%between the sheets%') AND 
                   (std_article.keywords NOT LIKE '%intercourse%' AND std_article.keywords NOT LIKE '%between the sheets%')  
                   and std_article.categoryid NOT LIKE '289'  GROUP BY std_article.id ORDER BY std_trend_hits1 DESC LIMIT 0, $limit ";

        return $article;

    }


}
