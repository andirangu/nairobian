@php extract($data) @endphp

@extends('layouts.app')

@section('content')

    <!--top carousel section-->
    <div class="container" style="margin-top: 150px!important;">

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 mb-5">
                        <div class="advert d-none d-lg-block mt-10">
                            <img src="
                            {{asset('assets/img/advert/mwananchi.jpg')}}" class="img-responsive">
                        </div>

                        <h3 style="color: #00508d; margin-top: 20px; text-transform: none; font-size: 45px!important; line-height: 45px">
                            <strong>{{$article ->title}}</strong></h3>

                        <div class="row">
                            <div class="col-md-2">
                                <p style="font-size: 50px; font-weight: lighter; color: #0a0a0a"><strong> 1.2K</strong>
                                </p>
                                <p style="line-height: 1px; margin-left: 16px">SHARES</p>
                            </div>

                            <div class="d-inline"><img src="{{asset('assets/img/articles/fb.jpg')}}" class="img-fluid"
                                                       style="height: 43px"></div>
                            <div class="d-inline ml-2"><img src="{{asset('assets/img/articles/tw.jpg')}}"
                                                            class="img-fluid"></div>
                            <div class="d-inline ml-2"><img src="{{asset('assets/img/articles/pin.jpg')}}"
                                                            class="img-fluid"></div>
                            <div class="d-inline ml-2"><img src="{{asset('assets/img/articles/google.jpg')}}"
                                                            class="img-fluid"></div>
                            <div class="d-inline ml-2"><img src="{{asset('assets/img/articles/mail.jpg')}}"
                                                            class="img-fluid"></div>
                        </div>

                        <div class="row">
                            <img src="{!! asset('/assets/img/post_photos/'.$article->thumbURL) !!}" class="img-fluid"
                                 style="height: 450px!important; width: 800px!important;">
                            <p style="background: #808080; color: #f1f1f1; width: 800px; font-size: 14px">
                                <i>{{$article->title}}</i></p>

                            <?php
                            $status = $article->storyType;
                            if ($status == 'paid' && Auth::guest()) {

                            ?>

                            <p data-toggle="modal" data-target="#myModal">
                                {{ str_limit($article->story, $limit = 400, $end = '...') }}
                                <span style="cursor: pointer;color: deepskyblue">Read More</span>
                            </p>

                            <?php

                            }else {
                            ?>

                            {{$article->story}}

                            <?php

                            }?>

                        </div>

                        <style>
                            .modal {
                                text-align: center;
                            }

                            @media screen and (min-width: 768px) {
                                .modal:before {
                                    display: inline-block;
                                    vertical-align: middle;
                                    content: " ";
                                    height: 100%;
                                }
                            }

                            .modal-dialog {
                                display: inline-block;
                                text-align: left;
                                vertical-align: middle;
                            }
                        </style>

                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                    </div>
                                    <div class="modal-body" style="margin-bottom: 10px!important;">
                                        <p style="font-size: 20px">Kindly login to continue reading this post</p>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal"
                                                        style="padding-bottom: 10px!important; padding-top: 10px!important;">
                                                    Close
                                                </button>
                                            </div>
                                            <div class="col-md-6 pull-right">
                                                <a href="{{route('login')}}"
                                                   class="btn btn-success"
                                                   style="text-decoration: none;padding-bottom: 10px!important; padding-top: 10px!important;">Login</a>

                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="advert d-none d-lg-block mt-10">
                            <img src="{{asset('assets/img/advert/mwananchi.jpg')}}" class="img-responsive">
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="row ml-2" style="border-bottom: #D3D3D3 1px solid; margin-bottom: 5px">
                            <div class="col-md-5 d-none d-lg-block">
                                <img src="{!! asset('/assets/img/post_photos/'.$tbt->thumbURL) !!}" alt=""
                                     style="height: 120px!important; width: 180px!important;">
                            </div>

                            <div class="col-md-7 d-none d-lg-block">
                                <p style="color: #2674b6;margin-left: 45px!important;">
                                    <a href="{{route('category',[$tbt->category->id,str_slug($tbt->category->name)])}}"
                                       style="color: #e60024">
                                        <strong>TBT</strong><span><small><strong>S</strong></small></span>: </span>
                                    </a>
                                    <a href="{{route('article',[$tbt->id,str_slug($tbt->title)])}}"
                                       style="color: #2674b6; text-decoration: none">
                                        <strong>{{$tbt->title}}</strong>
                                    </a>

                                </p>
                            </div>
                        </div>
                        <div class="row ml-2" style="border-bottom: #D3D3D3 1px solid; margin-bottom: 5px">
                            <div class="col-md-5 d-none d-lg-block">
                                <img src="{!! asset('/assets/img/post_photos/'.$betting->thumbURL) !!}" alt=""
                                     style="height: 120px!important; width: 180px!important;">
                            </div>
                            <div class="col-md-7 d-none d-lg-block">
                                <p style="color: #2674b6;margin-left: 45px!important;">
                                    <a href="{{route('category',[$betting->category->id,str_slug($betting->category->name)])}}"
                                       style="color: #e60024">
                                        <strong>BETTING</strong>:
                                    </a>
                                    <a href="{{route('article',[$betting->id,str_slug($betting->title)])}}"
                                       style="color: #2674b6; text-decoration: none">
                                        <strong>{{$betting->title}}</strong>
                                    </a>

                                </p>
                            </div>
                        </div>
                        <div class="row ml-2" style="border-bottom: #D3D3D3 1px solid; margin-bottom: 5px">
                            <div class="col-md-5 d-none d-lg-block">
                                <img src="{!! asset('/assets/img/post_photos/'.$health->thumbURL) !!}" alt=""
                                     style="height: 120px!important; width: 180px!important;">
                            </div>

                            <div class="col-md-7 d-none d-lg-block">
                                <p style="color: #2674b6;margin-left: 45px!important;">
                                    <a href="{{route('category',[$health->category->id,str_slug($health->category->name)])}}"
                                       style="color: #e60024">
                                        <strong>HEALTH</strong>:
                                    </a>
                                    <a href="{{route('article',[$health->id,str_slug($health->title)])}}"
                                       style="color: #2674b6; text-decoration: none">
                                        <strong>{{$health->title}}</strong>
                                    </a>

                                </p>
                            </div>
                        </div>
                        <div class="row ml-2" style="border-bottom: #D3D3D3 1px solid; margin-bottom: 5px">
                            <div class="col-md-5 d-none d-lg-block">
                                <img src="{!! asset('/assets/img/post_photos/'.$fashion->thumbURL) !!}" alt=""
                                     style="height: 120px!important; width: 180px!important;">
                            </div>
                            <div class="col-md-7 d-none d-lg-block">
                                <p style="color: #2674b6;margin-left: 45px!important;">
                                    <a href="{{route('category',[$fashion->category->id,str_slug($fashion->category->name)])}}"
                                       style="color: #e60024">
                                        <strong>FASHION</strong>:
                                    </a>
                                    <a href="{{route('article',[$fashion->id,str_slug($fashion->title)])}}"
                                       style="color: #2674b6; text-decoration: none">
                                        <strong>{{$fashion->title}}</strong>
                                    </a>

                                </p>
                            </div>
                        </div>

                        <div class="row ml-2" style="border-bottom: #D3D3D3 1px solid; margin-bottom: 5px">
                            <div class="col-md-5 d-none d-lg-block">
                                <img src="{!! asset('/assets/img/post_photos/'.$betting->thumbURL) !!}" alt=""
                                     style="height: 120px!important; width: 180px!important;">
                            </div>
                            <div class="col-md-7 d-none d-lg-block">
                                <p style="color: #2674b6;margin-left: 45px!important;">
                                    <a href="{{route('category',[$betting->category->id,str_slug($betting->category->name)])}}"
                                       style="color: #e60024">
                                        <strong>BETTING</strong>:
                                    </a>
                                    <a href="{{route('article',[$betting->id,str_slug($betting->title)])}}"
                                       style="color: #2674b6; text-decoration: none">
                                        <strong>{{$betting->title}}</strong>
                                    </a>

                                </p>
                            </div>
                        </div>
                        <div class="row ml-2" style="border-bottom: #D3D3D3 1px solid; margin-bottom: 5px">
                            <div class="col-md-5 d-none d-lg-block">
                                <img src="{!! asset('/assets/img/post_photos/'.$health->thumbURL) !!}" alt=""
                                     style="height: 120px!important; width: 180px!important;">
                            </div>

                            <div class="col-md-7 d-none d-lg-block">
                                <p style="color: #2674b6;margin-left: 45px!important;">
                                    <a href="{{route('category',[$health->category->id,str_slug($health->category->name)])}}"
                                       style="color: #e60024">
                                        <strong>HEALTH</strong>:
                                    </a>
                                    <a href="{{route('article',[$health->id,str_slug($health->title)])}}"
                                       style="color: #2674b6; text-decoration: none">
                                        <strong>{{$health->title}}</strong>
                                    </a>

                                </p>
                            </div>
                        </div>
                        <div class="row ml-2" style="border-bottom: #D3D3D3 1px solid; margin-bottom: 5px">
                            <div class="col-md-5 d-none d-lg-block">
                                <img src="{!! asset('/assets/img/post_photos/'.$fashion->thumbURL) !!}" alt=""
                                     style="height: 120px!important; width: 180px!important;">
                            </div>
                            <div class="col-md-7 d-none d-lg-block">
                                <p style="color: #2674b6;margin-left: 45px!important;">
                                    <a href="{{route('category',[$fashion->category->id,str_slug($fashion->category->name)])}}"
                                       style="color: #e60024">
                                        <strong>FASHION</strong>:
                                    </a>
                                    <a href="{{route('article',[$fashion->id,str_slug($fashion->title)])}}"
                                       style="color: #2674b6; text-decoration: none">
                                        <strong>{{$fashion->title}}</strong>
                                    </a>

                                </p>
                            </div>
                        </div>

                        <img src="{{asset('assets/img/advert/uni.jpg')}}" class="img-fluid ml-4 d-none d-lg-block mt-10"
                             style="height: 250px!important; width: 400px!important;">

                        <style>
                            #popular {
                                background: #00508d;
                                padding: 8px 8px;
                                font-size: 16px;
                                width: 100%;
                                margin-bottom: 10px;
                                margin-top: 20px;
                                color: #ffffff;
                                font-weight: bold;
                            }
                        </style>

                        <div class="button border-0 ml-4 mt-5" id="popular">Latest News</div>
                        <?php
                        for ($i = 1; $i < count($latests); $i++) {
                        $latest = $latests[$i];
                        ?>
                        <div class="row ml-2">
                            <div class="col-md-3">
                                <img src="{!! asset('/assets/img/post_photos/'.$latest->thumbURL) !!}" alt=""
                                     style="height: 70px!important; width: 100px!important;">
                            </div>

                            <div class="col-md-8 ml-3">
                                <p class="ml-1">
                                    <a href="{{route('article',[$latest->id,str_slug($latest->title)])}}"
                                       style="color: #000000; text-decoration: none">
                                        {{ str_limit($latest->title, $limit = 70, $end = '...') }}
                                    </a>
                                </p>
                            </div>
                        </div>
                        <?php } ?>


                    </div>


                </div>

            </div>
        </section>


        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 mb-5">
                        @if(count($trendings)>0)
                            <div class="row">
                                <?php

                                for ($i = 1; $i < count($latests); $i++) {
                                $latest = $latests[$i];
                                ?>
                                <div class="col-md-6">
                                    <figure class="figure">

                                        <img src="assets/img/category/mara2.jpg" class="figure-img img-fluid" alt=""
                                             style="height: 180px!important;">

                                        <h5 style="text-transform: none"><strong>Four of every 100 death is caused by
                                                alcohol.</strong></h5>

                                        <p style="color: #000000; font-size: 14px; line-height: 1px"><strong>Jane Doe
                                                <span
                                                        class="text-muted"
                                                        style="margin-left: 10px"> JULY 22, 2018</span></strong></p>

                                    </figure>
                                </div>
                                <?php } ?>
                            </div>

                            <h5><strong>YOU MAY ALSO LIKE</strong></h5>
                            <div class="row mt-2">

                                <div class="col-md-4">
                                    <figure class="figure">

                                        <img src="assets/img/category/mara2.jpg" class="figure-img img-fluid" alt=""
                                             style="height: 180px!important;">

                                        <h5 style="text-transform: none"><strong>Four of every 100 death is caused by
                                                alcohol.</strong></h5>

                                        <p style="color: #000000; font-size: 14px; line-height: 1px" class="text-muted">
                                            <span><strong>JULY 22, 2018</strong></span></p>

                                    </figure>
                                </div>
                                <div class="col-md-4">
                                    <figure class="figure">

                                        <img src="assets/img/category/mara2.jpg" class="figure-img img-fluid" alt=""
                                             style="height: 180px!important;">

                                        <h5 style="text-transform: none"><strong>Four of every 100 death is caused by
                                                alcohol.</strong></h5>

                                        <p style="color: #000000; font-size: 14px; line-height: 1px" class="text-muted">
                                            <span><strong>JULY 22, 2018</strong></span></p>

                                    </figure>
                                </div>
                                <div class="col-md-4">
                                    <figure class="figure">

                                        <img src="assets/img/category/mara2.jpg" class="figure-img img-fluid" alt=""
                                             style="height: 180px!important;">

                                        <h5 style="text-transform: none"><strong>Four of every 100 death is caused by
                                                alcohol.</strong></h5>

                                        <p style="color: #000000; font-size: 14px; line-height: 1px" class="text-muted">
                                            <span><strong>JULY 22, 2018</strong></span></p>

                                    </figure>
                                </div>
                            </div>
                        @endif

                    </div>

                    <div class="col-md-4 mt-5 mb-5 d-none d-lg-block mt-10">

                        <img src="{{asset('assets/img/advert/uni.jpg')}}" class="img-fluid ml-4"
                             style="height: 250px!important; width: 400px!important;">

                    </div>
                </div>

            </div>
        </section>


    </div>

    <!--top footer section-->

    <section>
        <div class="container d-none d-lg-block">
            <style>
                .heading-bottom-border {
                    width: 20% !important;
                    border-bottom-width: 10px;
                    padding-top: 5px !important;
                }
            </style>
            <div class="row">
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


@endsection