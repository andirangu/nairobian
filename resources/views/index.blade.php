@php extract($data) @endphp

@extends('layouts.app')

@section('content')

    <!--top carousel section-->
    <div class="container" style="margin-top: 150px!important;">

        <!--slider section-->
        <section>
            <div role="main" class="main mt-5">

                <div class="container">
                    <div class="row">

                        <div class="col">
                            <div class="owl-carousel owl-theme stage-margin"
                                 data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 20}">
                                <div>
                                    <div class="btn-danger mb-1">
                                        <a href="{{route('category',[$ted->category->id,str_slug($ted->category->name)])}}"
                                           style="color: #ffffff; text-decoration: none">
                                            UNCLE TED
                                        </a>
                                    </div>
                                    <img alt="" class="img-fluid rounded"
                                         style="height: 170px!important; width: 250px!important;"
                                         src="{!! asset('/assets/img/post_photos/'.$ted->thumbURL) !!}">
                                    <p>
                                        <a href="{{route('article',[$ted->id,str_slug($ted->title)])}}"
                                           style="color: #000000; text-decoration: none">
                                            <strong>{{$ted->title}}</strong>
                                        </a>
                                    </p>
                                </div>

                                <div>
                                    <div class="btn-success mb-1">
                                        <a href="{{route('category',[$story->category->id,str_slug($story->category->name)])}}"
                                           style="color: #ffffff; text-decoration: none">
                                            MY STORY
                                        </a>
                                    </div>
                                    <img alt="" class="img-fluid rounded"
                                         style="height: 170px!important; width: 250px!important;"
                                         src="{!! asset('/assets/img/post_photos/'.$story->thumbURL) !!}">
                                    <p>
                                        <a href="{{route('article',[$story->id,str_slug($story->title)])}}"
                                           style="color: #000000; text-decoration: none">
                                            <strong>{{$story->title}}</strong>
                                        </a>
                                    </p>
                                </div>

                                <div>
                                    <div class="btn-info mt-1">
                                        <a href="{{route('category',[$tbt->category->id,str_slug($tbt->category->name)])}}"
                                           style="color: #ffffff; text-decoration: none">
                                            TBT'S
                                        </a>
                                    </div>
                                    <img alt="" class="img-fluid rounded"
                                         style="height: 170px!important; width: 250px!important;"
                                         src="{!! asset('/assets/img/post_photos/'.$tbt->thumbURL) !!}">
                                    <p>
                                        <a href="{{route('article',[$tbt->id,str_slug($tbt->title)])}}"
                                           style="color: #000000; text-decoration: none">
                                            <strong>{{$tbt->title}}</strong>
                                        </a>
                                    </p>
                                </div>

                                <div>
                                    <div class="btn-warning mt-1">

                                        <a href="{{route('category',[$fashion->category->id,str_slug($fashion->category->name)])}}"
                                           style="color: #ffffff; text-decoration: none">
                                            FASHION
                                        </a>

                                    </div>
                                    <img alt="" class="img-fluid rounded"
                                         style="height: 170px!important; width: 250px!important;"
                                         src="{!! asset('/assets/img/post_photos/'.$fashion->thumbURL) !!}">
                                    <p>
                                        <a href="{{route('article',[$fashion->id,str_slug($fashion->title)])}}"
                                           style="color: #000000; text-decoration: none">
                                            <strong>{{$fashion->title}}</strong>
                                        </a>
                                    </p>
                                </div>

                                <div>
                                    <div class="btn-dark mt-1">
                                        <a href="{{route('category',[$politics->category->id,str_slug($politics->category->name)])}}"
                                           style="color: #ffffff; text-decoration: none">
                                            POLITICS
                                        </a>
                                    </div>
                                    <img alt="" style="height: 170px!important; width: 250px!important;"
                                         class="img-fluid rounded"
                                         src="{!! asset('/assets/img/post_photos/'.$politics->thumbURL) !!}">
                                    <p>
                                        <a href="{{route('article',[$politics->id,str_slug($politics->title)])}}"
                                           style="color: #000000; text-decoration: none">
                                            <strong>{{$politics->title}}</strong>
                                        </a>
                                    </p>
                                </div>

                                <div>
                                    <div class="btn-primary mt-1">
                                        <a href="{{route('category',[$health->category->id,str_slug($health->category->name)])}}"
                                           style="color: #ffffff; text-decoration: none">
                                            HEALTH
                                        </a>
                                    </div>
                                    <img alt="" class="img-fluid rounded"
                                         style="height: 170px!important; width: 250px!important;"
                                         src="{!! asset('/assets/img/post_photos/'.$health->thumbURL) !!}">
                                    <p>
                                        <a href="{{route('article',[$health->id,str_slug($health->title)])}}"
                                           style="color: #000000; text-decoration: none">
                                            <strong>{{$health->title}}</strong>
                                        </a>
                                    </p>
                                </div>

                                <div>
                                    <div class="btn-danger mt-1">
                                        <a href="{{route('category',[$gossip->category->id,str_slug($gossip->category->name)])}}"
                                           style="color: #ffffff; text-decoration: none">
                                            GOSSIPS
                                        </a>
                                    </div>
                                    <img alt="" class="img-fluid rounded"
                                         style="height: 170px!important; width: 250px!important;"
                                         src="{!! asset('/assets/img/post_photos/'.$gossip->thumbURL) !!}">
                                    <p>
                                        <a href="{{route('article',[$gossip->id,str_slug($gossip->title)])}}"
                                           style="color: #000000; text-decoration: none">
                                            <strong>{{$gossip->title}}</strong>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <!--advert section-->
        <section>
            <!-- advert -->
            <section>

                <div class="container d-none d-lg-block mt-10">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-9">
                            <img src="assets/img/advert/mwananchi.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </section>
        </section>

        <!--nairobian tv section-->
        <section>
            <div class="container mt-5">
                <div class="row">
                    <div class="col-md-5">
                        @php
                            $city = $cities[0];
                        @endphp
                        <figure class="figure">
                            <a href="{{route('article',[$city->id,str_slug($city->title)])}}"><img
                                        src="{!! asset('/assets/img/post_photos/'.$city->thumbURL) !!}"
                                        class="figure-img img-fluid rounded"
                                        alt="" style="height: 230px!important; width: 400px!important;"></a>
                            <h4 style="color: deepskyblue; text-transform: none">
                                <a href="{{route('article',[$city->id,str_slug($city->title)])}}"
                                   style="color: #000000; text-decoration: none">
                                    <strong>{{$city->title}}</strong>
                                </a>

                            </h4>
                        </figure>

                        <div class="row">
                            <?php
                            for ($i = 1;$i < count($cities);$i++){
                            $city = $cities[$i];
                            ?>
                            <div class="col-md-6">
                                <figure class="figure">
                                    <a href="{{route('article',[$city->id,str_slug($city->title)])}}"><img
                                                src="{!! asset('/assets/img/post_photos/'.$city->thumbURL) !!}"
                                                class="figure-img img-fluid rounded"
                                                alt="" style="height: 160px!important;"></a>
                                    <h5 style="color: deepskyblue; text-transform: none">
                                        <a href="{{route('article',[$city->id,str_slug($city->title)])}}"
                                           style="text-decoration: none">{{$city->title}}</a>
                                    </h5>
                                </figure>
                            </div>
                            <?php } ?>

                        </div>
                    </div>

                    <div class="col-md-3">
                        @foreach($videos as $video)
                            <div class="row">
                                <div class="btn-video mb-2">
                                    Video
                                </div>
                                <p class="border-bottom"><a
                                            href="{{'https://www.youtube.com/watch?v='.($video->videoURL)}}"
                                            class="text  text-dark" style="text-decoration: none" target="_blank">
                                        {{$video->title}}
                                    </a></p>
                            </div>
                        @endforeach
                    </div>

                    <div class="col-md-4">
                        <div class="fullscreen-bg">
                            <iframe class="embed-responsive-item"
                                    src="https://www.youtube.com/embed/live_stream?channel=UCKVsdeoHExltrWMuK0hOWmg&amp;autoplay=1"
                                    frameborder="0" allow="autoplay; encrypted-media" width="400px" height="250px"
                                    allowfullscreen=""></iframe>
                        </div>


                        <div style="margin-bottom: 20px!important; width: 350px!important; margin-left: 20px!important;">
                            <h4 style="margin-top: 20px; color: #00508f;"><strong>NAIROBIAN NEWSPAPER</strong></h4>
                            <div class="owl-carousel owl-theme show-nav-hover"
                                 data-plugin-options="{'items': 1, 'margin': 10,'loop': false, 'nav': true, 'dots': false}">
                                <div>
                                    <img alt="" class="img-fluid rounded"
                                         src="{{asset('assets/img/post_photos/Nairobian151214.jpg')}}"
                                         style="height: 250px!important;">
                                </div>
                                <div>
                                    <img alt="" class="img-fluid rounded"
                                         src="{{asset('assets/img/post_photos/images.jpg')}}"
                                         style="height: 250px!important;">
                                </div>
                                <div>
                                    <img alt="" class="img-fluid rounded"
                                         src="{{asset('assets/img/post_photos/news2.jpg')}}"
                                         style="height: 250px!important;">
                                </div>
                                <div>
                                    <img alt="" class="img-fluid rounded"
                                         src="{{asset('assets/img/post_photos/C-eHnRZXUAAaMAz.jpg')}}"
                                         style="height: 250px!important;">
                                </div>
                            </div>
                            <p style="color: #00508f; line-height: 1px!important; font-size: 18px">
                                <a href="https://newsstand.standardmedia.co.ke/" style="text-decoration: none"
                                   target="_blank">
                                    <strong>Read your copy now</strong>
                                </a>
                            </p>
                        </div>


                    </div>


                </div>
            </div>
        </section>
    </div>

    <!--politic, health and my story section-->
    <section class="section section-primary" style="background: #f1f1f1!important;">
        <div class="container" style="margin-bottom: 20px!important;">
            <div class="row">
                <div class="col-md-3" style="margin-top: 60px!important;">
                    <div class="card" style="height: 830px!important;">
                        <div class="card-body">

                            @php
                                $politic = $politicsCard[0];
                            @endphp

                            <h4 style="border-bottom: #8ec641 solid 3px!important; color: #8ec641; font-weight: bold"><a
                                        href="{{route('category',[$politic->category->id,str_slug($politic->category->name)])}}"
                                        style="text-decoration: none; color: #8ec641">
                                    POLITICS</a></h4>

                            <?php
                            for ($i = 1;$i < count($politicsCard);$i++){
                            $politic = $politicsCard[$i];
                            ?>


                            <a href="{{route('article',[$politic->id,str_slug($politic->title)])}}"><img
                                        class="card-img-top"
                                        src="{!! asset('/assets/img/post_photos/'.$politic->thumbURL) !!}"
                                        alt="image cap"></a>
                            <h4 class="card-subtitle mb-2"
                                style="color: #8ec641; margin-top: 10px; font-size: 20px!important;"><a
                                        href="{{route('article',[$politic->id,str_slug($politic->title)])}}"
                                        style="text-decoration: none; color: #8ec641">{{ str_limit($politic->title, $limit = 70, $end = '...') }}</a>
                            </h4>

                            <?php } ?>


                        </div>
                    </div>

                </div>

                <div class="col-md-3" style="margin-top: 60px!important;">
                    <div class="card" style="height: 830px!important;">
                        <div class="card-body">

                            @php
                                $health = $healthCard[0]
                            @endphp

                            <h4 style="border-bottom: #ef2f24 solid 3px!important; color: #ef2f24; font-weight: bold"><a
                                        href="{{route('category',[$health->category->id,str_slug($health->category->name)])}}"
                                        style="text-decoration: none; color: #ef2f24">
                                    HEALTH</a></h4>

                            <?php
                            for ($i = 1; $i < count($healthCard); $i++) {
                            $health = $healthCard[$i];
                            ?>

                            <a href="{{route('article',[$health->id,str_slug($health->title)])}}"><img
                                        class="card-img-top"
                                        src="{!! asset('/assets/img/post_photos/'.$health->thumbURL) !!}"
                                        alt="image cap"></a>

                            <h4 class="card-subtitle mb-2"
                                style="color: #ef2f24; margin-top: 10px; font-size: 20px!important;"><a
                                        href="{{route('article',[$health->id,str_slug($health->title)])}}"
                                        style="text-decoration: none; color: #ef2f24">{{ str_limit($health->title, $limit = 70, $end = '...') }}</a>


                            </h4>

                            <?php } ?>


                        </div>
                    </div>

                </div>

                <div class="col-md-3" style="margin-top: 60px!important;">
                    <div class="card" style="height: 830px!important;">
                        <div class="card-body">

                            @php
                                $myStory = $myStoryCard[0]
                            @endphp

                            <h4 style="border-bottom: #23c2e2 solid 3px!important; color: #23c2e2; font-weight: bold"><a
                                        href="{{route('category',[$myStory->category->id,str_slug($myStory->category->name)])}}"
                                        style="text-decoration: none; color: #23c2e2;">
                                    MY STORY</a></h4>


                            <?php
                            for ($i = 1; $i < count($myStoryCard); $i++) {
                            $myStory = $myStoryCard[$i];
                            ?>

                            <a href="{{route('article',[$myStory->id,str_slug($myStory->title)])}}"><img
                                        class="card-img-top"
                                        src="{!! asset('/assets/img/post_photos/'.$myStory->thumbURL) !!}"
                                        alt="image cap"></a>
                            <h4 class="card-subtitle mb-2"
                                style="color: #23c2e2; margin-top: 10px; font-size: 20px!important;"><a
                                        href="{{route('article',[$myStory->id,str_slug($myStory->title)])}}"
                                        style="text-decoration: none; color: #23c2e2;">{{ str_limit($myStory->title, $limit = 70, $end = '...') }}</a>
                            </h4>

                            <?php } ?>

                        </div>
                    </div>

                </div>

                <div class="col-md-3" style="margin-top: 60px!important;">
                    <div class="card d-none d-lg-block">
                        <div class="card-body">

                            <img class="card-img-top" src="assets/img/advert/smart.jpg" alt="Card image cap">

                            @php
                                $gossip = $gossips[0]
                            @endphp

                            <div class="button border-0" id="gossip">GOSSIP</div>


                            <?php
                            for ($i = 1; $i < count($gossips); $i++) {
                            $gossip = $gossips[$i];
                            ?>

                            <div class="row mt-2">

                                <style>
                                    a:hover {
                                        color: #ff3300;
                                    }
                                </style>

                                <div class="post-container ml-3 mr-3">
                                    <div class="post-thumb">
                                        <a href="{{route('article',[$gossip->id,str_slug($gossip->title)])}}">
                                            <img src="{!! asset('/assets/img/post_photos/'.$gossip->thumbURL) !!}"
                                                 alt=""
                                                 style="height: 90px!important; margin-top: 10px!important; margin-left: 2px!important;">
                                        </a>
                                    </div>
                                    <div class="post-content">
                                        <p class="ml-1"><a
                                                    href="{{route('article',[$gossip->id,str_slug($gossip->title)])}}"
                                                    style="color: #ffffff; text-decoration: none"> {{ str_limit($gossip->title, $limit = 70, $end = '...') }}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <?php } ?>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section>

    <!--advert section-->
    <section>

        <div class="container d-none d-lg-block"
             style="margin-top: 40px!important; margin-bottom: 30px!important;">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-9">
                    <img src="assets/img/advert/mwananchi.jpg" class="img-responsive">
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </section>

    <!--events section-->
    <section>
        <section class="section section-primary mt-0" style="background: #23c2e2!important;">
            <div class="container">
                <div class="row">

                    @php
                        $event = $events[0]
                    @endphp

                    <div class="col" style="margin-bottom: 20px!important;">
                        <h4 style="margin-top: 20px"><a
                                    href="{{route('category',[$event->category->id,str_slug($event->category->name)])}}"
                                    style="color: #ffffff; font-weight: bold; text-decoration: none">EVENTS</a>
                        </h4>
                        <div class="owl-carousel owl-theme show-nav-hover"
                             data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">


                            <?php
                            for ($i = 1; $i < count($events); $i++) {
                            $event = $events[$i];
                            ?>

                            <div>
                                <img alt="" class="img-fluid rounded"
                                     src="{!! asset('/assets/img/post_photos/'.$event->thumbURL) !!}"
                                     style="width: 430px!important;">
                            </div>
                            <?php } ?>

                        </div>
                    </div>


                </div>
            </div>
        </section>
    </section>

    <!--uncle section-->
    <section>
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-6">

                    @php
                        $uncle = $uncles[1];
                    @endphp

                    <h4 style="color: #2674b6;"><strong><a
                                    href="{{route('category',[$uncle->category->id,str_slug($uncle->category->name)])}}">
                                UNCLE TED</a></strong></h4>


                    <figure class="figure">
                        <a href="{{route('article',[$uncle->id,str_slug($uncle->title)])}}"><img
                                    src="{!! asset('/assets/img/post_photos/'.$uncle->thumbURL) !!}"
                                    class="figure-img img-fluid"
                                    alt="" style="height: 280px!important; width: 515px!important;"></a>
                        <h4 style="color: deepskyblue; text-transform: none"><a
                                    href="{{route('article',[$uncle->id,str_slug($uncle->title)])}}"
                                    style="text-decoration: none; color: deepskyblue">{{$uncle->title}}</a>
                        </h4>

                    </figure>


                    <div class="row mr-2">

                        <?php
                        for ($i = 2;$i < count($uncles);$i++){
                        $uncle = $uncles[$i];
                        ?>

                        <div class="col-md-6">
                            <figure class="figure">
                                <a href="{{route('article',[$uncle->id,str_slug($uncle->title)])}}"><img
                                            src="{!! asset('/assets/img/post_photos/'.$uncle->thumbURL) !!}"
                                            class="figure-img img-fluid"
                                            alt="" style="height: 160px!important;"></a>
                                <h4 style="color: deepskyblue; text-transform: none"><a
                                            href="{{route('article',[$uncle->id,str_slug($uncle->title)])}}"
                                            style="text-decoration: none">{{$uncle->title}}</a>
                                </h4>
                            </figure>
                        </div>
                        <?php } ?>

                    </div>

                </div>

                <div class="col-md-3">
                    @php
                        $tbt = $tbts[0];
                    @endphp
                    <figure class="figure ml-1 border-bottom">
                        <?php
                        for ($i = 1;$i < count($tbts);$i++){
                        $tbt = $tbts[$i];
                        ?>

                        <img src="{!! asset('/assets/img/post_photos/'.$tbt->thumbURL) !!}" class="figure-img img-fluid"
                             alt="" style="height: 180px!important;">
                        <h4 style="color: #3975b4; text-transform: none"><span style="color: #ef3026"> <a
                                        href="{{route('category',[$tbt->category->id,str_slug($tbt->category->name)])}}"
                                        style="text-decoration: none; color: red">TBT'S - paid:</a> </span>
                            <a href="{{route('article',[$tbt->id,str_slug($tbt->title)])}}"
                               style="text-decoration: none">{{$tbt->title}}</a></h4>

                        <p>{{ str_limit($tbt->story, $limit = 120, $end = '...') }}..<a
                                    href="{{route('article',[$tbt->id,str_slug($tbt->title)])}}">Read More</a>
                        </p>
                        <?php } ?>
                    </figure>

                    @php
                        $fashion = $fashions[1];
                    @endphp

                    <figure class="figure ml-1 border-bottom">
                        <?php
                        for ($i = 2;$i < count($fashions);$i++){
                        $fashion = $fashions[$i];
                        ?>
                        <img src="{!! asset('/assets/img/post_photos/'.$fashion->thumbURL) !!}"
                             class="figure-img img-fluid"
                             alt="" style="height: 180px!important;">
                        <h4 style="color: #3975b4; text-transform: none"><span
                                    style="color: #ef3026"> Fashions: </span><a
                                    href="{{route('article',[$fashion->id,str_slug($fashion->title)])}}"
                                    style="text-decoration: none">{{$fashion->title}}</a>
                        </h4>
                        <p>{{ str_limit($fashion->story, $limit = 120, $end = '...') }}..<a
                                    href="{{route('article',[$fashion->id,str_slug($fashion->title)])}}">Read More</a>
                        </p>
                        <?php } ?>
                    </figure>


                </div>
                <div class="col-md-3">

                    @php
                        $sport = $sports[0];
                    @endphp

                    <figure class="figure ml-1 border-bottom">
                        <?php
                        for ($i = 1;$i < count($sports);$i++){
                        $sport = $sports[$i];
                        ?>
                        <img src="{!! asset('/assets/img/post_photos/'.$sport->thumbURL) !!}"
                             class="figure-img img-fluid"
                             alt="" style="height: 180px!important;">
                        <h4 style="color: #3975b4; text-transform: none"><span style="color: #ef3026">Sports: </span>
                            <a
                                    href="{{route('article',[$sport->id,str_slug($sport->title)])}}"
                                    style="text-decoration: none">{{$sport->title}}</a>
                        </h4>
                        <p>{{ str_limit($sport->story, $limit = 120, $end = '...') }}..<a
                                    href="{{route('article',[$sport->id,str_slug($sport->title)])}}">Read More</a></p>
                        <?php } ?>
                    </figure>


                    @php
                        $food = $foods[0];
                    @endphp

                    <figure class="figure ml-1 border-bottom">
                        <?php
                        for ($i = 1;$i < count($foods);$i++){
                        $food = $foods[$i];
                        ?>
                        <img src="{!! asset('/assets/img/post_photos/'.$food->thumbURL) !!}"
                             class="figure-img img-fluid"
                             alt="" style="height: 180px!important;">
                        <h4 style="color: #3975b4; text-transform: none"><span style="color: #ef3026"> Food: </span>
                            <a
                                    href="{{route('article',[$food->id,str_slug($food->title)])}}"
                                    style="text-decoration: none">{{$food->title}}</a>
                        </h4>
                        <p>{{ str_limit($food->story, $limit = 120, $end = '...') }}..<a
                                    href="{{route('article',[$food->id,str_slug($food->title)])}}">Read More</a></p>
                        <?php } ?>
                    </figure>


                </div>

            </div>
        </div>
    </section>


    <!--category section-->

    <section>
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-8">

                    @php
                        $money = $moneys[0];
                    @endphp

                    <h4 style="color: #2674b6;"><strong><a
                                    href="{{route('category',[$money->category->id,str_slug($money->category->name)])}}"
                                    style="text-decoration: none">MONEY</a></strong>
                    </h4>
                    <div class="row border-bottom">
                        <?php
                        for ($i = 0;$i < count($moneys);$i++){
                        $money = $moneys[$i];
                        ?>
                        <div class="col-md-4">
                            <figure class="figure">
                                <a href="{{route('article',[$money->id,str_slug($money->title)])}}"><img
                                            src="{!! asset('/assets/img/post_photos/'.$money->thumbURL) !!}"
                                            class="figure-img img-fluid"
                                            alt="" style="height: 160px!important;"></a>
                                <p><strong><a href="{{route('article',[$money->id,str_slug($money->title)])}}"
                                              style="text-decoration: none">
                                            {{$money->title}}</a></strong></p>
                            </figure>
                        </div>
                        <?php } ?>

                    </div>


                    @php
                        $betting = $bettings[0];
                    @endphp

                    <h4 style="color: #2674b6; margin-top: 10px!important;">
                        <strong>
                            <a href="{{route('category',[$betting->category->id,str_slug($betting->category->name)])}}"
                               style="text-decoration: none">
                                BETTING
                            </a>
                        </strong>
                    </h4>
                    <div class="row border-bottom">
                        <?php
                        for ($i = 0;$i < count($bettings);$i++){
                        $betting = $bettings[$i];
                        ?>
                        <div class="col-md-4">
                            <figure class="figure">
                                <a href="{{route('article',[$betting->id,str_slug($betting->title)])}}"><img
                                            src="{!! asset('/assets/img/post_photos/'.$betting->thumbURL) !!}"
                                            class="figure-img img-fluid"
                                            alt="" style="height: 160px!important;"></a>
                                <p><strong><a href="{{route('article',[$betting->id,str_slug($betting->title)])}}"
                                              style="text-decoration: none">
                                            {{$betting->title}}</a></strong></p>
                            </figure>
                        </div>
                        <?php } ?>

                    </div>


                    <div class="row d-none d-lg-block" style="border-top: #e60024 2px solid">

                        @php
                            $nairobiShop = $nairobiShops[0];
                        @endphp

                        <h4 style="color: #2674b6; margin-top: 15px">
                            <strong>
                                <a href="{{route('category',[$nairobiShop->category->id,str_slug($nairobiShop->category->name)])}}"
                                   style="text-decoration: none">
                                    NAIROBI SHOP
                                </a>
                            </strong>
                        </h4>


                        <div class="row">
                            <?php
                            for ($i = 0;$i < count($nairobiShops);$i++){
                            $nairobiShop = $nairobiShops[$i];
                            ?>
                            <div class="col-md-4">
                                <figure class="figure">
                                    <a href="{{route('article',[$nairobiShop->id,str_slug($nairobiShop->title)])}}">
                                        <img src="{!! asset('/assets/img/post_photos/'.$nairobiShop->thumbURL) !!}"
                                             class="figure-img img-fluid"
                                             alt="" style="height: 160px!important;">
                                    </a>
                                    <p style="color: #2674b6;">
                                        <strong>
                                            <span style="color: #e60024">
                                                <a href="{{route('category',[$story->category->id,str_slug($story->category->name)])}}">
                                                    Sales:
                                                </a>
                                            </span>
                                            <a href="{{route('article',[$nairobiShop->id,str_slug($nairobiShop->title)])}}"
                                               style="text-decoration: none">
                                                {{$nairobiShop->title}}
                                            </a>
                                        </strong>
                                    </p>
                                </figure>
                            </div>
                            <?php } ?>


                        </div>

                    </div>


                </div>

                <div class="col-md-4 mt-5 d-none d-lg-block">
                    <figure class="figure">
                        <img src="assets/img/advert/ad.jpg" class="figure-img img-fluid"
                             alt="" style="width: 400px!important; height: 300px">
                    </figure>

                    <style>
                        #defender {
                            background: #8dc641;
                            padding: 8px 8px;
                            font-size: 14px;
                            width: 100%;
                            margin-bottom: 10px;
                            margin-top: 10px;
                            color: #ffffff;
                            font-weight: bold;
                            text-align: center;
                        }

                        .defender {
                            background: #8dc641;
                            padding: 3px 3px;
                            font-size: 13px;
                            width: 30%;
                            color: #ffffff;
                            font-weight: bold;
                            margin-right: 5px;
                            text-align: center;
                        }

                        .case {
                            background: #e40019;
                            padding: 3px 3px;
                            font-size: 13px;
                            width: 30%;
                            color: #ffffff;
                            font-weight: bold;
                            margin-right: 5px;
                            text-align: center;
                        }

                    </style>

                    <div class="button border-0" id="defender">THE NAIROBIAN DEFENDER</div>

                    @foreach ($defenders as $defender)
                        <div class="row mt-2">
                            <div class="col-md-3">
                                <img src="{!! asset('/assets/img/post_photos/'.$defender->thumbURL) !!}" alt=""
                                     style="height: 80px!important; width: 100px!important;">
                            </div>

                            <div class="col-md-8 ml-3">
                                <p class="ml-1">
                                <span class="defender">
                                    <a href="{{route('category',[$defender->category->id,str_slug($defender->category->name)])}}"
                                       style="text-decoration: none; color: #ffffff">THE DEFENDER</a>
                                </span>
                                    <a href="{{route('article',[$defender->id,str_slug($defender->title)])}}"
                                       style="color: #0a0a0a; text-decoration: none">
                                        {{$defender->title}}
                                    </a>
                                </p>

                            </div>
                        </div>
                    @endforeach

                    <figure class="figure">
                        <img src="assets/img/advert/sportpesa.jpg" class="figure-img img-fluid"
                             alt="" style="width: 400px!important; height: 250px">
                    </figure>

                </div>

            </div>
        </div>
        </div>
    </section>

    <!--top footer section-->
    <section>
        <div class="container d-none d-lg-block">
            <style>
                .list-primary {
                    padding-top: 5px !important;
                    padding-bottom: 5px !important;
                }
            </style>
            <div class="row" style="margin-top: 30px!important;">
                @foreach ($menus as $menu)
                    <div class="col-md-3">

                        <ul class="list list-icons list-primary" style="line-height: 1px!important;">
                            <li style="color: #7197c8;">
                                <i class="fas fa-star"
                                   style="line-height: 1px!important; margin-bottom: 10px!important;"></i>
                                <a href="{{route('category',[$menu->id,str_slug($menu->name)])}}"
                                   style="text-decoration: none">
                                    {!!$menu->name!!}
                                </a>
                            </li>
                        </ul>

                    </div>
                @endforeach
            </div>

        </div>
    </section>

@endsection