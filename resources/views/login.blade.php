@extends('layouts.app')

@section('content')

    <div class="container" style="margin-top: 150px!important;">

        <div class="row">
            <div class="col">

                <style>
                    #login-btn {

                        padding: 10px 10px;
                        font-size: 16px;
                        width: 40%
                    }

                    #register-btn {

                        padding: 10px 10px;
                        font-size: 16px;
                        width: 40%
                    }
                </style>

                <div class="featured-boxes">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="featured-box featured-box-primary text-left mt-5">
                                <div class="box-content">
                                    <h4 class="heading-primary text-uppercase mb-3">I'm a Returning Customer</h4>

                                    @if(Session::has('message'))
                                        <p class="alert {{ Session::get('alert-danger', 'alert-danger') }}">
                                            {{ Session::get('message') }}
                                        </p>
                                    @endif


                                    <form action="{{ route('login') }}" method="post">

                                        {{csrf_field()}}

                                        <div class="form-row">
                                            <div class="form-group col {{ $errors->has('email') ? ' has-error' : '' }}">

                                                @if ($errors->has('email'))
                                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                                @endif

                                                <label>E-mail Address</label>
                                                <input type="text" name="email" class="form-control form-control-lg"
                                                       value="{{ old('email') }}">
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col {{ $errors->has('password') ? ' has-error' : '' }}">

                                                @if ($errors->has('password'))
                                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                                @endif

                                                <label>Password</label>
                                                <input type="password" name="password" class="form-control form-control-lg">
                                            </div>
                                        </div>

                                        <div class="form-row">

                                            <div class="form-group col-lg-6">
                                                <div class="form-inline">
                                                    <label class="form-check-label">
                                                        <a href="register">No account? Create one</a>
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="form-group col-lg-6">
                                                <input type="submit" id="login-btn" value="Login"
                                                       class="btn btn-primary float-right mb-5"
                                                       data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection