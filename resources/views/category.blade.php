@php extract($data) @endphp

@extends('layouts.app')

@section('content')

    @if(count($articles)>0)

        <div class="container" style="margin-top: 150px!important;">
            <!--health section-->

            @php
                $article = $articles->first();
            @endphp

            <section>
                <div class="container mt-5">

                    @php
                        $article = $articles[0];
                    @endphp

                    <div class="row">
                        <div class="col-md-5">
                            <h3 style="color: #00508d; text-transform: uppercase">
                                <strong>{{$article->category->name}}</strong>
                            </h3>
                            <figure class="figure">
                                <h4 style="color: #00508d; text-transform: none">
                                    <strong>
                                        <a href="{{route('article',[$article->id,str_slug($article->title)])}}"
                                           style="text-decoration: none">
                                            {{$article->title}}
                                        </a>
                                    </strong>
                                </h4>
                                <p style="line-height: 1px!important; font-size: 14px!important; color: #00508d;">
                                    <strong>{{$article->category->name}}</strong>
                                    <span class="border-left" style="color: #6c6666!important;">
                                    <strong>
                                        {{$article->publishdate->diffForHumans()}}
                                    </strong>
                                </span>
                                    <span class="border-left" style="color: #6c6666!important;">
                                    <strong>By: {{$article->author}}</strong>
                                </span>

                                </p>
                                <a href="{{route('article',[$article->id,str_slug($article->title)])}}"
                                   style="text-decoration: none"><img
                                            src="{!! asset('/assets/img/post_photos/'.$article->thumbURL) !!}"
                                            class="figure-img img-fluid"
                                            alt="" style="height: 270px!important;"></a>

                            </figure>

                            <div class="row">

                                <?php
                                for ($i = 1;$i < count($articles);$i++){
                                $article = $articles[$i];
                                ?>

                                <div class="col-md-6 d-none d-lg-block">
                                    <figure class="figure">
                                        <a href="{{route('article',[$article->id,str_slug($article->title)])}}"
                                           style="text-decoration: none"><img
                                                    src="{!! asset('/assets/img/post_photos/'.$article->thumbURL) !!}"
                                                    class="figure-img img-fluid"
                                                    alt="" style="height: 150px!important; width: 190px!important;"></a>

                                        <span style="color: #6c6666!important; font-size: 14px!important;">
                                            <strong>
                                                {{$article->publishdate->diffForHumans()}}
                                            </strong>
                                        </span>

                                        <span class="border-left"
                                              style="color: #6c6666!important; font-size: 14px!important;"> <strong> By: {{$article->author}}</strong></span>

                                        </p>

                                        <h4 style="text-transform: none">
                                            <strong>
                                                <a href="{{route('article',[$article->id,str_slug($article->title)])}}"
                                                   style="text-decoration: none; color: #000000">
                                                    {{$article->title}}
                                                </a>
                                            </strong></h4>
                                    </figure>
                                </div>
                                <?php } ?>
                            </div>

                        </div>

                        <div class="col-md-3">
                            <div class="row">
                                <img src="{{asset('assets/img/advert/china.jpg')}}" class="img-fluid mr-5">
                            </div>

                            <div class="row mt-4">
                                <h4 style="color: #00508d; text-transform: none"><strong>Latest News</strong></h4>

                                @foreach($latestArticles as $latestarticle)
                                    <div class="item-new">
                                        <p class="border-bottom">
                                            <strong>{{$latestarticle->title}}
                                            </strong>
                                        </p>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                        <div class="col-md-4">


                            <div class="row ml-1">
                                <div class="fullscreen-bg">
                                    <iframe class="embed-responsive-item"
                                            src="https://www.youtube.com/embed/live_stream?channel=UCKVsdeoHExltrWMuK0hOWmg&amp;autoplay=1"
                                            frameborder="0" allow="false; encrypted-media" width="350px"
                                            height="250px"
                                            allowfullscreen=""></iframe>
                                </div>
                            </div>

                            <div class="row mt-5 ml-3">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="3">
                                            <h4>
                                                <strong>
                                                    Today's Programme
                                                </strong>
                                            </h4>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Afri Screen</td>
                                        <td>14:00:00 - 15:59:00</td>
                                    </tr>
                                    <tr>
                                        <td>Mbiu ya KTN</td>
                                        <td>14:00:00 - 15:59:00</td>
                                    </tr>
                                    <tr>
                                        <td>Lilo & Stitch</td>
                                        <td>14:00:00 - 15:59:00</td>
                                    </tr>
                                    <tr>
                                        <td>Lilo & Stitch</td>
                                        <td>14:00:00 - 15:59:00</td>
                                    </tr>
                                    <tr>
                                        <td>Lilo & Stitch</td>
                                        <td>14:00:00 - 15:59:00</td>
                                    </tr>
                                    <tr>
                                        <td>Lilo & Stitch</td>
                                        <td>14:00:00 - 15:59:00</td>
                                    </tr>
                                    <tr>
                                        <td>Lilo & Stitch</td>
                                        <td>14:00:00 - 15:59:00</td>
                                    </tr>

                                    <thead>
                                    <tr>
                                        <th colspan="3">
                                            <h4>
                                                <strong>
                                                    Today's Programme
                                                </strong>
                                            </h4>
                                        </th>
                                    </tr>
                                    </thead>

                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </section>

            @php
                $articles = $articles2->forget(0);
            @endphp

            <section>
                <div class="container">
                    <div class="row">

                        <div class="col-md-8">
                            <div class="row">
                                @foreach($articles2 as $article)
                                    <div class="col-md-6">
                                        <figure class="figure">
                                            <img src="{!! asset('/assets/img/post_photos/'.$article->thumbURL) !!}"
                                                 class="figure-img img-fluid"
                                                 alt="" style="height: 270px!important;">

                                            <h4 style="text-transform: none">
                                                <strong>
                                                    <a href="{{route('article',[$article->id,str_slug($article->title)])}}"
                                                       style="text-decoration: none; color: #000000">
                                                        {{$article->title}}
                                                    </a>
                                                </strong>
                                            </h4>

                                            <p style="line-height: 1px!important; font-size: 14px!important; color: #00508d;">
                                                <strong>HEALTH</strong>

                                                <span class="border-left"
                                                      style="color: #6c6666!important;">
                                                    <strong>
                                                        {{$article->publishdate->diffForHumans()}}
                                                    </strong>
                                                </span>

                                                <span class="border-left"
                                                      style="color: #6c6666!important;">
                                                    <strong>
                                                        By: {{$article->author}}
                                                    </strong>
                                                </span>
                                            </p>

                                            <p>{{ str_limit($article->story, $limit = 120, $end = '...') }}..<a
                                                        href="{{route('article',[$article->id,str_slug($article->title)])}}">Read
                                                    More</a>

                                        </figure>
                                    </div>
                                @endforeach
                            </div>


                        </div>

                        <style>
                            #popular {
                                background: #00508d;
                                padding: 8px 8px;
                                font-size: 16px;
                                width: 100%;
                                margin-bottom: 10px;
                                margin-top: 10px;
                                color: #ffffff;
                                font-weight: bold;
                            }

                            .defender {
                                background: #8dc641;
                                padding: 3px 3px;
                                font-size: 13px;
                                width: 30%;
                                color: #ffffff;
                                font-weight: bold;
                                margin-right: 5px;
                                text-align: center;
                            }

                            .case {
                                background: #e40019;
                                padding: 3px 3px;
                                font-size: 13px;
                                width: 30%;
                                color: #ffffff;
                                font-weight: bold;
                                margin-right: 5px;
                                text-align: center;
                            }

                        </style>

                        {{--most popular--}}
                        <div class="col-md-4">
                            <img src="{{asset('assets/img/advert/uni.jpg')}}" class="img-fluid">

                            <div class="button border-0" id="popular">Most Popular</div>

                            @if(count($populars)>0)

                                @foreach($populars as $article)
                                    <img src="{{asset('assets/img/category/young.jpg')}}" class="img-fluid">
                                    <p>Caption</p>
                                @endforeach

                            @else
                                <style>
                                    .alert {
                                        height: 300px;
                                    }
                                </style>
                                <div class="alert alert-warning" role="alert">
                                    Sorry. No article found
                                </div>
                            @endif

                            @foreach($populars as $article)
                                <div class="row mt-2 border-bottom">
                                    <div class="col-md-3">
                                        <img src="assets/img/gossip/g1.jpg" alt=""
                                             style="height: 80px!important; width: 100px!important;">
                                    </div>

                                    <div class="col-md-8 ml-3 d-none d-lg-block">
                                        <p class="ml-1"> Using color to add meaning only
                                            provides a visual indication, which will </p>
                                    </div>
                                </div>
                            @endforeach

                            <div class="button border-0" id="popular">
                                <a href="{{route('category',[$defenderArticles[0]->category->id,str_slug($defenderArticles[0]->category->name)])}}"
                                   style="text-decoration: none; color: #ffffff">THE DEFENDER</a>
                            </div>

                            @foreach($defenderArticles as $defender)
                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <img src="{!! asset('/assets/img/post_photos/'.$defender->thumbURL) !!}" alt=""
                                             style="height: 80px!important; width: 100px!important;">
                                    </div>

                                    <div class="col-md-8 ml-3">
                                        <p class="ml-1">
                                            <span class="defender"> THE DEFENDER </span>
                                            <a href="{{route('article',[$defender->id,str_slug($defender->title)])}}"
                                               style="color: #0a0a0a; text-decoration: none">
                                                {{$defender->title}}
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            @endforeach


                        </div>

                    </div>

                </div>
            </section>
        </div>

        <!--events section-->
        <section>
            <section class="section section-primary mt-0" style="background: #23c2e2!important;">
                <div class="container">
                    <div class="row">

                        @php
                            $event = $events[0]
                        @endphp

                        <div class="col" style="margin-bottom: 20px!important;">
                            <h4 style="margin-top: 20px"><a
                                        href="{{route('category',[$event->category->id,str_slug($event->category->name)])}}"
                                        style="color: #ffffff; font-weight: bold; text-decoration: none">EVENTS</a>
                            </h4>
                            <div class="owl-carousel owl-theme show-nav-hover"
                                 data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">


                                <?php
                                for ($i = 1; $i < count($events); $i++) {
                                $event = $events[$i];
                                ?>

                                <div>
                                    <img alt="" class="img-fluid rounded"
                                         src="{!! asset('/assets/img/post_photos/'.$event->thumbURL) !!}"
                                         style="width: 430px!important;">
                                </div>
                                <?php } ?>

                            </div>
                        </div>


                    </div>
                </div>
            </section>
        </section>

        <section>
            <div class="container mt-5">
                <div class="row">

                    {{--load more section--}}
                    <div class="col-md-8">
                        <div class="row" id="post-data">
                            @foreach($loadmorearticles as $article)
                                <div class="col-md-6 border-bottom">
                                    <figure class="figure">

                                        <img src="{!! asset('/assets/img/post_photos/'.$article->thumbURL) !!}"
                                             class="figure-img img-fluid"
                                             alt="" style="height: 270px!important;">

                                        <h4 style="text-transform: none">
                                            <strong>
                                                <a href="{{route('article',[$article->id,str_slug($article->title)])}}"
                                                   style="text-decoration: none; color: #000000">
                                                    {{$article->title}}
                                                </a>
                                            </strong>
                                        </h4>

                                        <p style="line-height: 1px!important; font-size: 14px!important; color: #00508d;">
                                            <strong>HEALTH</strong>

                                            <span class="border-left"
                                                  style="color: #6c6666!important;">
                                                <strong>
                                                    {{$article->publishdate->diffForHumans()}}
                                                </strong>
                                            </span>

                                            <span class="border-left"
                                                  style="color: #6c6666!important;">
                                                <strong>
                                                    By: {{$article->author}}
                                                </strong>
                                            </span>
                                        </p>

                                        <p>{{ str_limit($article->story, $limit = 120, $end = '...') }}..<a
                                                    href="{{route('article',[$article->id,str_slug($article->title)])}}">Read
                                                More</a>

                                    </figure>
                                </div>
                            @endforeach
                            <style>
                                .btn {
                                    margin-left: 250px;
                                    margin-top: 15px;
                                    background: #8dc641;
                                    color: #ffffff;
                                    border-radius: 20px;
                                    padding-right: 50px;
                                    padding-left: 50px;
                                }
                            </style>

                            <div class="btn d-none d-lg-block">Load More</div>

                        </div>


                        <script type="text/javascript">
                            var page = 1;
                            $(window).scroll(function () {
                                if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                                    page++;
                                    loadMoreData(page);
                                }
                            });


                            function loadMoreData(page) {
                                $.ajax(
                                    {
                                        url: '?page=' + page,
                                        type: "get",
                                        beforeSend: function () {
                                            $('.ajax-load').show();
                                        }
                                    })
                                    .done(function (data) {
                                        if (data.html == " ") {
                                            $('.ajax-load').html("No more records found");
                                            return;
                                        }
                                        $('.ajax-load').hide();
                                        $("#post-data").append(data.html);
                                    })
                                    .fail(function (jqXHR, ajaxOptions, thrownError) {
                                        alert('server not responding...');
                                    });
                            }
                        </script>


                        {{--related stories section--}}
                        <div class="row d-none d-lg-block border-bottom">
                            <h4 style="color: #2674b6; margin-top: 15px" class="border-bottom"><strong>Related
                                    Stories</strong></h4>

                            <div class="row">
                                <?php
                                for ($i = 0;$i < count($nairobiShops);$i++){
                                $nairobiShop = $nairobiShops[$i];
                                ?>
                                <div class="col-md-4">
                                    <figure class="figure">
                                        <a href="{{route('article',[$nairobiShop->id,str_slug($nairobiShop->title)])}}">
                                            <img src="{!! asset('/assets/img/post_photos/'.$nairobiShop->thumbURL) !!}"
                                                 class="figure-img img-fluid"
                                                 alt="" style="height: 160px!important;">
                                        </a>
                                        <p style="color: #2674b6;">
                                            <strong>
                                            <span style="color: #e60024">
                                                <a href="{{route('category',[$nairobiShop->category->id,str_slug($nairobiShop->category->name)])}}">
                                                    Sales:
                                                </a>
                                            </span>
                                                <a href="{{route('article',[$nairobiShop->id,str_slug($nairobiShop->title)])}}"
                                                   style="text-decoration: none">
                                                    {{$nairobiShop->title}}
                                                </a>
                                            </strong>
                                        </p>
                                    </figure>
                                </div>
                                <?php } ?>


                            </div>


                        </div>

                    </div>

                    {{--gossip section--}}
                    <div class="col-md-4">

                        <img src="{{asset('assets/img/advert/uni.jpg')}}" class="img-fluid">

                        <div class="button border-0" style="background: #fba919" id="popular">
                            Gossips
                        </div>

                        @foreach($gossipArticles as $article)
                            <div class="row mt-2 ml-1 mr-1" style="background: #eeeeee">
                                <div class="col-md-7">
                                    <p class="ml-1">

                                        <a href="{{route('article',[$article->id,str_slug($article->title)])}}"
                                           style="color: #0a0a0a; text-decoration: none">
                                            {{ str_limit($article->story, $limit = 60, $end = '...') }}
                                        </a>

                                </div>
                                <div class="col-md-4 mr-3">
                                    <img src="{!! asset('/assets/img/post_photos/'.$article->thumbURL) !!}" alt=""
                                         style="height: 90px!important; width: 130px!important;">
                                </div>
                            </div>
                        @endforeach


                    </div>

                </div>
            </div>
        </section>

    @else
        <style>
            .alert {
                margin-top: 150px;
                height: 50px;
            }
        </style>
        <div class="container">
            <div class="alert alert-warning" role="alert">
                Sorry. No article found
            </div>
        </div>

    @endif

    <!--top footer section-->

    <section>
        <div class="container d-none d-lg-block">
            <style>
                .heading-bottom-border {
                    width: 20% !important;
                    border-bottom-width: 10px;
                    padding-top: 5px !important;
                }
            </style>
            <div class="row">
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="heading heading-primary heading-border heading-bottom-border">
                        <h5 style="font-size: 14px!important; margin-bottom: 0!important;">NEWS</h5>
                    </div>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                    <ul class="list list-icons list-primary" style="line-height: 5px!important;">
                        <li style="color: #7197c8"><i class="fas fa-check" style="line-height: 2px!important;"></i>
                            Breaking
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

@endsection