@extends('layouts.app')

@section('content')

    <div class="container" style="margin-top: 150px!important;">

        <div class="row">
            <div class="col">

                <style>
                    #login-btn {

                        padding: 10px 10px;
                        font-size: 16px;
                        width: 40%
                    }

                    #register-btn {

                        padding: 10px 10px;
                        font-size: 16px;
                        width: 40%
                    }
                </style>

                <div class="featured-boxes">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="featured-box featured-box-primary text-left mt-5">
                                <div class="box-content">
                                    <h4 class="heading-primary text-uppercase mb-3">Register An Account</h4>

                                    <form action="{{ route('register') }}" method="post">
                                        {{csrf_field()}}


                                        <div class="form-row">
                                            <div class="form-group col-lg-6 {{ $errors->has('name') ? ' has-error' : '' }}">

                                                @if ($errors->has('name'))
                                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                                @endif

                                                <label>Name</label>
                                                <input type="text" name="name" class="form-control form-control-lg">
                                            </div>


                                            <div class="form-group col-lg-6 {{ $errors->has('phone') ? ' has-error' : '' }}">

                                                @if ($errors->has('phone'))
                                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                                @endif

                                                <label>Phone</label>
                                                <input type="text" name="phone"
                                                       class="form-control form-control-lg">
                                            </div>

                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col {{ $errors->has('email') ? ' has-error' : '' }}">

                                                @if ($errors->has('email'))
                                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif

                                                <label>Email Address</label>
                                                <input type="text" name="email" class="form-control form-control-lg"
                                                       value="{{ old('email') }}">
                                            </div>
                                        </div>


                                        <div class="form-row">
                                            <div class="form-group col-lg-12 {{ $errors->has('password') ? ' has-error' : '' }}">

                                                @if ($errors->has('password'))
                                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                @endif

                                                <label>Password</label>
                                                <input type="password" name="password" class="form-control form-control-lg">
                                            </div>

                                        </div>


                                        <div class="form-row">

                                            <div class="form-group col-lg-6">
                                                <div class="form-inline">
                                                    <label class="form-check-label">
                                                        <a href="login">Already have account? Login Here</a>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <input type="submit" value="Register" id="register-btn"
                                                       class="btn btn-primary float-right mb-5"
                                                       data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection




