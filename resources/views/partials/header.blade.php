<header id="header"
        data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 55, 'stickySetTop': '-55px', 'stickyChangeLogo': true}">
    <div class="header-body fixed-top">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="{{url('/')}}">
                                <img alt="Porto" width="250" height="70" data-sticky-width="82"
                                     data-sticky-height="40"
                                     data-sticky-top="33" src="{{asset('assets/img/logo.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row pt-3">

                        <div class="header-search d-none d-md-block">
                            <form id="searchForm"
                                  action="https://preview.oklerthemes.com/porto/6.2.1/page-search-results.html"
                                  method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search..."
                                           required>
                                    <span class="input-group-append">
													<button class="btn btn-light" type="submit"><i
                                                                class="fas fa-search"></i></button>
												</span>
                                </div>
                            </form>
                        </div>


                    </div>
                    <div class="header-row">
                        <div class="header-nav">
                            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                <nav class="collapse">

                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a href="{{url('/')}}">
                                                Home
                                            </a>
                                        </li>


                                        @foreach ($menus as $menu)
                                            <li class="dropdown">
                                                <a href="{{route('category',[$menu->id,str_slug($menu->name)])}}">
                                                    {!!$menu->name!!}
                                                </a>

                                                <ul class="dropdown-menu">
                                                    @foreach ($menu->subCategories() as $childNav)
                                                        <li>
                                                            <a class="dropdown-item"
                                                               href="{{route('category',[$childNav->id,str_slug($childNav->name)])}}">
                                                                {!!$childNav->name!!}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>

                                            </li>
                                        @endforeach


                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="#">
                                                MORE
                                            </a>
                                            <ul class="dropdown-menu">

                                                @foreach ($moreNavs as $moreNav)
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item" href="#">{!!$moreNav->name!!}</a>
                                                        <ul class="dropdown-menu">
                                                            @foreach ($moreNav->subCategories() as $childNav)
                                                                <li>
                                                                    <a class="dropdown-item"
                                                                       href="{{route('category',
                                                                       [$childNav->id,str_slug($childNav->name)])}}">
                                                                        {!!$childNav->name!!}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        </li>

                                        <li class="border-left">
                                            <a href="index.php">
                                                Nairobian TV<i class="fa fa-play"
                                                               style="margin-left: 4px!important; color: red!important;"></i>
                                            </a>
                                        </li>

                                        <!-- Authentication Links -->
                                        @if (Auth::guest())

                                            <li class="dropdown dropdown-mega dropdown-mega-signin signin border-left"
                                                id="headerAccount">
                                                <a class="dropdown-item dropdown-toggle" href="{{route('login')}}">
                                                    <i class="fas fa-user"></i> Sign In
                                                </a>
                                            </li>

                                        @else

                                            <li class="dropdown dropdown-mega dropdown-mega-signin signin logged border-left"
                                                id="headerAccount">
                                                <a class="dropdown-item dropdown-toggle">
                                                    <i class="fas fa-user"></i>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <div class="dropdown-mega-content">

                                                            <div class="row">
                                                                <div class="col-lg-8">
                                                                    <div class="user-avatar">
                                                                        <div class="img-thumbnail d-block">
                                                                            <img src="assets/img/clients/client-1.jpg"
                                                                                 alt="">
                                                                        </div>
                                                                        <p>
                                                                            <span>Welcome</span>
                                                                            <strong>{{Auth::user()->name}}</strong>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <ul class="list-account-options">
                                                                        <li>
                                                                            <a href="{{route('logout')}}">Logout</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>

                                        @endif
                                    </ul>


                                    </ul>
                                </nav>
                            </div>

                            <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                    data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
