<!--footer section-->
<section class="section section-primary" style="background: #f1f1f1!important;">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4 class="mt-5 text-black-50">Find us on</h4>
                <ul class="social-icons mb-5">
                    <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank"
                                                         title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank"
                                                        title="Twitter"><i class="fab fa-twitter"></i></a></li>
                    <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank"
                                                         title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
            </div>

            <div class="col-md-4">
                <div class="newsletter">
                    <h4 style="color: #000000; margin-top:30px">Sign up for the Newsletter</h4>
                    <p style="color: #000000;">Keep up on our always evolving product features and technology. Enter
                        your e-mail and subscribe to our newsletter.</p>

                </div>
            </div>

            <div class="col-md-5" style=" margin-top:60px">


                <div class="newsletter">

                    <form action=#" method="POST">
                        <div class="input-group">
                            <input class="form-control form-control-sm"
                                   type="text">
                            <span class="input-group-append">
											<button class="btn btn-light" type="submit">Go!</button>
										</span>
                        </div>
                    </form>
                </div>


            </div>

        </div>

    </div>
</section>

<div class="container-fluid" style="background: #3a75b7">
    <div class="row">
        <div class="col-lg-5"></div>
        <div class="col-lg-6">
            <p class="align-content-center" style="color: #ffffff; margin-top: 5px">© Copyright 2018. Standard Media
                Group.</p>
        </div>
        <div class="col-lg-1"></div>
    </div>
</div>


</div>