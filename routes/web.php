<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/', 'HomeController@index')->name('/');
Route::get('articles/{id}/{title}', 'ArticlesController@show')->name('article');
Route::get('categories/{id}/{title}', 'CategoriesController@show')->name('category');

Route::get('login', 'LoginController@showLogin')->name('login');
Route::post('login', 'LoginController@doLogin');
Route::get('logout', 'LoginController@doLogout')->name('logout');

Route::get('register', 'RegisterController@showRegister')->name('register');
Route::post('register', 'RegisterController@register');
